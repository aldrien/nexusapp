require 'test_helper'

class CommonUser::AbilitiesControllerTest < ActionDispatch::IntegrationTest
  test "should get change_password" do
    get common_user_abilities_change_password_url
    assert_response :success
  end

end
