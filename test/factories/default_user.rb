FactoryGirl.define do
  factory :default_user, :class => User do |f|
    f.sequence(:login) { |n| "Aldrien#{n}" }
    f.first_name 'Aldrien'
    f.last_name 'Test'
    f.sequence(:email) { |n| "default#{n}@gmail.com" }
    f.user_type_id 2
    f.password 'password'
    f.password_confirmation { |u| u.password }
    f.activated true
    f.activate_totp true
    f.activate_u2f false
    f.totp_secret 'z5fprtfloy5dreg4'
    f.photo File.open("#{Rails.root.join('features', 'upload_files', 'sample-profile.png')}", 'r')
  end
end