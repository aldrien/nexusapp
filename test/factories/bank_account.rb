FactoryGirl.define do
  factory :bank_account do |f|
    f.association :user, factory: :default_user
    f.description 'sample bank account description'
    f.bank 'Metro Bank'
    f.opening_balance 1000.00
    f.balance 500.00
    f.bank_type 'Merchant'
    f.sort_code 'ZX-SD-12'
  end
end
