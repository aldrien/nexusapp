FactoryGirl.define do
  factory :credit_transaction_category, class: 'TransactionCategory' do |f|
    f.title 'Credit Category'
    f.description 'Sample credit transaction category description'
    f.credit true
  end

  factory :debit_transaction_category, class: 'TransactionCategory' do |f|
    f.title 'Debit Category'
    f.description 'Sample debit transaction category description'
    f.credit false
  end
end