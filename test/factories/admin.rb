FactoryGirl.define do
  factory :admin, :class => User do |f|
    f.first_name 'Aldrien'
    f.last_name 'Test'
    f.sequence(:login) { |n| "admin#{n}" }
    f.sequence(:email) { |n| "admin#{n}@gmail.com" }
    f.user_type_id 1
    f.password 'password'
    f.password_confirmation { |u| u.password }
    f.activated true
    f.activate_totp true
    f.activate_u2f false
    f.totp_secret 'z5fprtfloy5dreg4'
  end
end
