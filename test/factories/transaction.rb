FactoryGirl.define do
  factory :bank_transaction, class: 'Transaction' do |f|
    f.association :bank_account, :factory => :bank_account
    f.amount 300.00
    f.description 'Sample transaction'
    f.association :transaction_category, :factory => :credit_transaction_category
    f.transaction_date Time.now
  end

  factory :new_credit_transaction, class: 'Transaction' do |f|
    f.credit true
    f.amount 30.00
    f.description 'Sample credit transaction'
    f.association :transaction_category, :factory => :credit_transaction_category
    f.transaction_date Time.now
  end

  factory :new_debit_transaction, class: 'Transaction' do |f|
    f.credit false
    f.amount 10.00
    f.description 'Sample debit transaction'
    f.association :transaction_category, :factory => :debit_transaction_category
    f.transaction_date Time.now
  end
end