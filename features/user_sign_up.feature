Feature: Users can sign up

  Scenario: User successfully sign up
    When I go to the home page
    And I follow "Sign Up"
    And I fill in "user_login" with "bob"
    And I fill in "user_first_name" with "Bob"
    And I fill in "user_last_name" with "Bobsen"
    And I fill in "user_email" with "bob@gmail.com"
    And I fill in "user_password" with "test"
    And I fill in "user_password_confirmation" with "test"
    And I press "Submit"
    Then I should see "Please set up your Two-Factor Authentication."
    When I load that new user to a variable
    And I initialize ROTP
    And I fill in "code" with the generated key
    And I press "Submit"
    Then I should see "Congratulations! You are one step away, please continue ..."
    And I should see "Should you ever lose your phone, below is recovery code that can be used to regain access to your account."
    And I should see "Please copy & save it in a safe place, or you will lose access to your account."
    And I should see user TOTP recovery code
    And I press "Proceed"
    Then I should see "Two Factor Authentication was successfully configured."

  Scenario: User sign up, invalid code
    When I go to the home page
    And I follow "Sign Up"
    And I fill in "user_login" with "bob"
    And I fill in "user_first_name" with "Bob"
    And I fill in "user_last_name" with "Bobsen"
    And I fill in "user_email" with "bob@gmail.com"
    And I fill in "user_password" with "test"
    And I fill in "user_password_confirmation" with "test"
    And I press "Submit"
    Then I should see "Please set up your Two-Factor Authentication."
    And I fill in "code" with "123456"
    And I press "Submit"
    Then I should see "Sorry that code didn't match code. Please try again!"

  Scenario: Blank Username/Login
    When I go to the home page
    And I follow "Sign Up"
    And I fill in "user_first_name" with "Bob"
    And I fill in "user_last_name" with "Bobsen"
    And I fill in "user_email" with "bob@gmail.com"
    And I fill in "user_password" with "test"
    And I fill in "user_password_confirmation" with "test"
    And I press "Submit"

  Scenario: Blank First name
    When I go to the home page
    And I follow "Sign Up"
    And I fill in "user_login" with "bob"
    And I fill in "user_last_name" with "Bobsen"
    And I fill in "user_email" with "bob@gmail.com"
    And I fill in "user_password" with "test"
    And I fill in "user_password_confirmation" with "test"
    And I press "Submit"
    Then I should see "First name can't be blank"

  Scenario: Blank Last name
    When I go to the home page
    And I follow "Sign Up"
    And I fill in "user_login" with "bob"
    And I fill in "user_first_name" with "Bob"
    And I fill in "user_email" with "bob@gmail.com"
    And I fill in "user_password" with "test"
    And I fill in "user_password_confirmation" with "test"
    And I press "Submit"
    Then I should see "Last name can't be blank"

  Scenario: Blank Email
    When I go to the home page
    And I follow "Sign Up"
    And I fill in "user_login" with "bob"
    And I fill in "user_first_name" with "Bob"
    And I fill in "user_last_name" with "Bobsen"
    And I fill in "user_password" with "test"
    And I fill in "user_password_confirmation" with "test"
    And I press "Submit"
    Then I should see "Email can't be blank"

  Scenario: Invalid Email
    When I go to the home page
    And I follow "Sign Up"
    And I fill in "user_login" with "bob"
    And I fill in "user_first_name" with "Bob"
    And I fill in "user_last_name" with "Bobsen"
    And I fill in "user_email" with "bob@gmail"
    And I fill in "user_password" with "test"
    And I fill in "user_password_confirmation" with "test"
    And I press "Submit"
    Then I should see "Email is invalid"

  Scenario: Already registered Login/Username and Email
    Given I have default user
    When I go to the home page
    And I follow "Sign Up"
    And I fill in "user_login" with default user login
    And I fill in "user_first_name" with "Aldrien"
    And I fill in "user_last_name" with "Bobsen"
    And I fill in "user_email" with default user email
    And I fill in "user_password" with "test"
    And I fill in "user_password_confirmation" with "test"
    And I press "Submit"
    Then I should see "Login Username has already registered."
    Then I should see "Email has already been taken"

  Scenario: Blank Password
    When I go to the home page
    And I follow "Sign Up"
    And I fill in "user_login" with "bob"
    And I fill in "user_first_name" with "Bob"
    And I fill in "user_last_name" with "Bobsen"
    And I fill in "user_email" with "bob@gmail.com"
    And I press "Submit"
    Then I should see "Password can't be blank"

  Scenario: Not Matched Password
    When I go to the home page
    And I follow "Sign Up"
    And I fill in "user_login" with "bob"
    And I fill in "user_first_name" with "Bob"
    And I fill in "user_last_name" with "Bobsen"
    And I fill in "user_email" with "bob@gmail.com"
    And I fill in "user_password" with "test1"
    And I fill in "user_password_confirmation" with "test2"
    And I press "Submit"
    Then I should see "Password confirmation doesn't match Password"