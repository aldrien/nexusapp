Feature: User can disable and re-enable Two Factor Authentication
  As a user
  I want to be able to disable and re-enable 2FA
  
  Background: I have a logged in user
    Given I have default user
    And that "default_user" logs in
    And that "default_user" enter valid pin code

  Scenario: User can disable 2FA
    When I follow "Security Settings"
    Then I should see "Two-Factor Authentication (2FA) Set Up"
    And I press "Disable Two-Factor Authentication"
    Then I should see "Disable Two-Factor Authentication (2FA)"
    And I fill in "disable_2fa_password" with "default_user" valid password
    And I press "Disable 2FA"
    Then I should see "Two Factor Authentication was successfully deactivated."

 Scenario: User cannot disable 2FA with invalid password
    When I follow "Security Settings"
    Then I should see "Two-Factor Authentication (2FA) Set Up"
    And I press "Disable Two-Factor Authentication"
    Then I should see "Disable Two-Factor Authentication (2FA)"
    And I fill in "disable_2fa_password" with "invalid password"
    And I press "Disable 2FA"
    Then I should see "Sorry, invalid password. Please try again!"

 Scenario: User can disable and re-enable 2FA
    When I follow "Security Settings"
    Then I should see "Two-Factor Authentication (2FA) Set Up"
    And I press "Disable Two-Factor Authentication"
    Then I should see "Disable Two-Factor Authentication (2FA)"
    And I fill in "disable_2fa_password" with "default_user" valid password
    And I press "Disable 2FA"
    Then I should see "Two Factor Authentication was successfully deactivated."
    And I follow "Enable Two-Factor Authentication"
    Then I should see "Please set up your Two-Factor Authentication."
    When I initialize ROTP
    And I fill in "code" with the generated key
    And I press "Submit"
    Then I should see "Congratulations! You are one step away, please continue ..."
    And I press "Proceed >>"
    Then I should see "Two Factor Authentication was successfully configured."
    And I should see "Welcome to Dashboard"

  Scenario: User can disable and re-enable 2FA, but invalid code
    When I follow "Security Settings"
    Then I should see "Two-Factor Authentication (2FA) Set Up"
    And I press "Disable Two-Factor Authentication"
    Then I should see "Disable Two-Factor Authentication (2FA)"
    And I fill in "disable_2fa_password" with "default_user" valid password
    And I press "Disable 2FA"
    Then I should see "Two Factor Authentication was successfully deactivated."
    And I follow "Enable Two-Factor Authentication"
    Then I should see "Please set up your Two-Factor Authentication."
    And I fill in "code" with "000000"
    And I press "Submit"
    Then I should see "Sorry that code didn't match code. Please try again!"

  Scenario: User can Reset 2FA
    When I follow "Security Settings"
    Then I should see "Two-Factor Authentication (2FA) Set Up"
    And I press "Reset Two-Factor Authentication"
    Then I should see "Disable Two-Factor Authentication (2FA)"
    And I fill in "reset_2fa_password" with "default_user" valid password
    And I press "Reset 2FA"
    Then I should see "Please set up your Two-Factor Authentication."
    When I initialize ROTP
    And I fill in "code" with the generated key
    And I press "Submit"
    Then I should see "Congratulations! You are one step away, please continue ..."
    And I press "Proceed >>"
    Then I should see "Two Factor Authentication was successfully configured."
    And I should see "Welcome to Dashboard"

  Scenario: User can Reset 2FA, but invalid password
    When I follow "Security Settings"
    Then I should see "Two-Factor Authentication (2FA) Set Up"
    And I press "Reset Two-Factor Authentication"
    Then I should see "Disable Two-Factor Authentication (2FA)"
    And I fill in "reset_2fa_password" with "invalid password"
    And I press "Reset 2FA"
    Then I should see "Sorry, invalid password. Please try again!"

  Scenario: User can Reset 2FA, but invalid code
    When I follow "Security Settings"
    Then I should see "Two-Factor Authentication (2FA) Set Up"
    And I press "Reset Two-Factor Authentication"
    Then I should see "Disable Two-Factor Authentication (2FA)"
    And I fill in "reset_2fa_password" with "default_user" valid password
    And I press "Reset 2FA"
    Then I should see "Please set up your Two-Factor Authentication."
    When I initialize ROTP
    And I fill in "code" with "000000"
    And I press "Submit"
    Then I should see "Sorry that code didn't match code. Please try again!"