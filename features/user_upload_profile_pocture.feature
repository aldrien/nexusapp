Feature: User can upload profile picture
  As a user
  I want to be able to upload and update my profile picture

  Background: Have a user
    Given I have default user

  Scenario: User logged in can upload profile picture
    When I go to the user login page
    And I fill in login with the correct "default_user" login
    And I fill in password with the correct "default_user" password
    And I press "Log In"
    Then I should see "Two-Factor Authentication (2FA)"
    When I initialize ROTP
    And I fill in "code" with the generated key
    And I press "Submit"
    Then I should see "You have successfully logged in."
    And I follow "My Profile"
    Then I should see "Want to change photo?"
    And I upload new image
    And I press "Upload New Profile Image"
    Then I should see "Your profile was successfully updated."
    And I should have new user photo
    And I should see the new photo in page
    
  Scenario: User logged in tries to upload profile photo but invalid file
    When I go to the user login page
    And I fill in login with the correct "default_user" login
    And I fill in password with the correct "default_user" password
    And I press "Log In"
    Then I should see "Two-Factor Authentication (2FA)"
    When I initialize ROTP
    And I fill in "code" with the generated key
    And I press "Submit"
    Then I should see "You have successfully logged in."
    And I follow "My Profile"
    Then I should see "Want to change photo?"
    And I upload invalid file type
    And I press "Upload New Profile Image"
    Then I should see "1 error prohibited this user from being saved:"
    Then I should see "You are not allowed to upload "csv" files, allowed types: jpg, jpeg, gif, png"
    