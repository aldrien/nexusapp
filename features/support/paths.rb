module NavigationHelpers
  # This file maps names to paths, and is used by
  # When /^I go to (.+)$/ do |page_name|
  
  def path_to(page_name)
    case page_name
    when /the home page/
      root_path
    when /the user login page/
      login_path
    when /the new user page/
      signup_path
    when /the dashboard page/
      dashboard_index_path
    when /the verify TOTP code/
      verify_totp_code_sessions_path
    when /the set up TOTP page/
      setup_totp_users_path
    when /the admin manage user page/
      admin_users_path
    when /the not found page/
      '/no/routes/page'
    when /the admin bank account page/
      admin_bank_accounts_path  
    when /the admin manage transaction page/
      admin_transactions_path
    when /the admin manage transaction category page/
      admin_transaction_categories_path
    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join("_").to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\nTry adding a mapping in #{__FILE__}"
      end
    end
  end
end

World(NavigationHelpers)
