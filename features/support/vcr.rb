require 'vcr'

VCR.configure do |c|
  c.hook_into :webmock
  c.cassette_library_dir     = 'features/vcr_cassettes'
  c.default_cassette_options = {
    :match_requests_on => [:method, :host, :path]
    # :record => :new_episodes,
    # :erb => true
  }
  c.allow_http_connections_when_no_cassette = true
end

VCR.cucumber_tags do |t|
  t.tag  '@localhost_request' # uses default record mode since no options are given
  t.tags '@disallowed_1', '@disallowed_2', :record => :none
  t.tag  '@vcr', use_scenario_name: true
  # re_record_interval: 4.weeks.to_i 
  # allow_unused_http_interactions: false
end

# Notes:
# You can also have VCR name your cassettes automatically according to the feature
# and scenario name by providing :use_scenario_name => true to #tag or #tags.

# Reference url: https://relishapp.com/vcr/vcr/v/3-0-3/docs/test-frameworks/usage-with-cucumberi