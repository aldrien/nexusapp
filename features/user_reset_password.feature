Feature: Users can reset their password
  As a user
  I want to be able to reset my passowrd
  
  Background:
    Given I have default user
    
  Scenario: Reset Password
    When I go to the user login page
    And I fill in login with the correct "default_user" login
    And I fill in password with the correct "default_user" password
    And I press "Log In"
    Then I should see "Two-Factor Authentication (2FA)"
    When I initialize ROTP
    And I fill in "code" with the generated key
    And I press "Submit"
    Then I should see "You have successfully logged in."
    And I follow "Change Password"
    And I fill in "old_password" with "default_user" old password
    And I fill in "user_password" with "newpass"
    And I fill in "user_password_confirmation" with "newpass"
    And I press "Change Password"
    Then I should see "Your password has been changed."

  Scenario: Reset Password, Invalid Old Password
    When I go to the user login page
    And I fill in login with the correct "default_user" login
    And I fill in password with the correct "default_user" password
    And I press "Log In"
    Then I should see "Two-Factor Authentication (2FA)"
    When I initialize ROTP
    And I fill in "code" with the generated key
    And I press "Submit"
    Then I should see "You have successfully logged in."
    And I follow "Change Password"
    And I fill in "old_password" with "wrongpassword"
    And I fill in "user_password" with "newpass"
    And I fill in "user_password_confirmation" with "newpass"
    And I press "Change Password"
    Then I should see "Old password was not entered correctly."
    
  Scenario: Reset Password, mismatched passwords
    When I go to the user login page
    And I fill in login with the correct "default_user" login
    And I fill in password with the correct "default_user" password
    And I press "Log In"
    Then I should see "Two-Factor Authentication (2FA)"
    When I initialize ROTP
    And I fill in "code" with the generated key
    And I press "Submit"
    And I follow "Change Password"
    And I fill in "old_password" with "default_user" old password
    And I fill in "user_password" with "test1"
    And I fill in "user_password_confirmation" with "test2"
    And I press "Change Password"
    Then I should see "Password confirmation doesn't match Password"

  Scenario: Reset Password, Blank Old Password
    When I go to the user login page
    And I fill in login with the correct "default_user" login
    And I fill in password with the correct "default_user" password
    And I press "Log In"
    Then I should see "Two-Factor Authentication (2FA)"
    When I initialize ROTP
    And I fill in "code" with the generated key
    And I press "Submit"
    Then I should see "You have successfully logged in."
    And I follow "Change Password"
    And I fill in "user_password" with "newpass"
    And I fill in "user_password_confirmation" with "newpass"
    And I press "Change Password"
    Then I should see "Old password was not entered correctly"

  Scenario: Reset Password, Blank Password
    When I go to the user login page
    And I fill in login with the correct "default_user" login
    And I fill in password with the correct "default_user" password
    And I press "Log In"
    Then I should see "Two-Factor Authentication (2FA)"
    When I initialize ROTP
    And I fill in "code" with the generated key
    And I press "Submit"
    Then I should see "You have successfully logged in."
    And I follow "Change Password"
    And I fill in "user_password_confirmation" with "newpass"
    And I press "Change Password"
    Then I should see "Password and confirmation cannot be blank."

  Scenario: Reset Password, Blank Password Confirmation
    When I go to the user login page
    And I fill in login with the correct "default_user" login
    And I fill in password with the correct "default_user" password
    And I press "Log In"
    Then I should see "Two-Factor Authentication (2FA)"
    When I initialize ROTP
    And I fill in "code" with the generated key
    And I press "Submit"
    Then I should see "You have successfully logged in."
    And I follow "Change Password"
    And I fill in "old_password" with "wrongpassword"
    And I fill in "user_password" with "newpass"
    And I press "Change Password"
    Then I should see "Password and confirmation cannot be blank."

  Scenario: Reset Password, Blank Password and Password Confirmation
    When I go to the user login page
    And I fill in login with the correct "default_user" login
    And I fill in password with the correct "default_user" password
    And I press "Log In"
    Then I should see "Two-Factor Authentication (2FA)"
    When I initialize ROTP
    And I fill in "code" with the generated key
    And I press "Submit"
    Then I should see "You have successfully logged in."
    And I follow "Change Password"
    And I fill in "old_password" with "wrongpassword"
    And I press "Change Password"
    Then I should see "Password and confirmation cannot be blank."