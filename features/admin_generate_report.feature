Feature: Admin can generate reports
  As an admin
  I want to be able to generate report and statement

  Background: I have an admin logged in
    Given I have admin user
    And that "admin" logs in
    And that "admin" enter valid pin code
    And I have bank account

  Scenario: Admin can generate report
    Given I have "300" credit transaction this month
    And I have "100" debit transaction this month
    When I follow "Report"
    Then I should see "A Summary of Deposit Account"
    And I should see my chequing bank account
    And I should see my total opening balance
    And I should see "Money into your account (1 items)"
    And I should see "Money out of your account (1 items)"
    And I should see this month closing balance
    And I should see "Total Income"
    And I should see "Total Expenses"

  Scenario: Admin can generate report, no transaction, filter by last month
    Given I have "300" credit transaction this month
    And I have "100" debit transaction this month
    When I follow "Report"
    And I select previous month
    And I press "Filter Result"
    Then I should see "A Summary of Deposit Account"
    And I should see my chequing bank account
    And I should see my total opening balance
    And I should see account balance forward
    And I should see "Money into your account (0 items)"
    And I should see "Money out of your account (0 items)"
    And I should have "£ 0.00" total "income"
    And I should have "£ 0.00" total "expenses"

  Scenario: Admin can generate report, has one credit transaction, filter by last month
    Given I have "300" credit transaction last month
    When I follow "Report"
    And I select previous month
    And I press "Filter Result"
    Then I should see "A Summary of Deposit Account"
    And I should see my chequing bank account
    And I should see my total opening balance
    And I should see account balance forward
    And I should see "Money into your account (1 items)"
    And I should see "Money out of your account (0 items)"
    And I should have "£ 300.00" total "income"

  Scenario: Admin can generate report, has one debit transaction, filter by last month
    Given I have "100" debit transaction last month
    When I follow "Report"
    And I select previous month
    And I press "Filter Result"
    Then I should see "A Summary of Deposit Account"
    And I should see my chequing bank account
    And I should see my total opening balance
    And I should see account balance forward
    And I should see "Money into your account (0 items)"
    And I should see "Money out of your account (1 items)"
    And I should have "£ 100.00" total "expenses"

  Scenario: Admin can generate report, has one credit and debit transactions, filter by last month
    Given I have "300" credit transaction last month
    And I have "100" debit transaction last month
    When I follow "Report"
    And I select previous month
    And I press "Filter Result"
    Then I should see "A Summary of Deposit Account"
    And I should see my chequing bank account
    And I should see my total opening balance
    And I should see account balance forward
    And I should see "Money into your account (1 items)"
    And I should see "Money out of your account (1 items)"
    And I should have "£ 300.00" total "income"
    And I should have "£ 100.00" total "expenses"
    And I should see last month closing balance

  Scenario: Admin can generate report, filter by credit category
    Given I have "300" credit transaction this month
    And I have "100" debit transaction this month
    When I follow "Report"
    And I select "Credit Category" from "category"
    And I press "Filter Result"
    Then I should see "A Summary of Deposit Account"
    And I should see my chequing bank account
    And I should see my total opening balance
    And I should see "Money into your account (1 items)"
    And I should not see "Total Income:"
    And I should not see "Total Expenses:"
    And I should not see "£ 100.00"
    And I should have "£ 300.00" for category "Credit Category"

  Scenario: Admin can generate report, filter by debit category
    Given I have "300" credit transaction this month
    And I have "100" debit transaction this month
    When I follow "Report"
    And I select "Debit Category" from "category"
    And I press "Filter Result"
    Then I should see "A Summary of Deposit Account"
    And I should see my chequing bank account
    And I should see my total opening balance
    And I should see "Money out of your account (1 items)"
    And I should not see "Total Income:"
    And I should not see "Total Expenses:"
    And I should not see "£ 300.00"
    And I should have "£ 100.00" for category "Debit Category"

  Scenario: Admin can generate report, filter by bank account
    Given I have another bank account
    And I have "300" credit transaction this month
    And I have "100" debit transaction this month
    When I follow "Report"
    And I select first bank account
    And I press "Filter Result"
    Then I should see "A Summary of Deposit Account"
    And I should see that user bank account
    And I should see my chequing bank account
    And I should see my total opening balance
    And I should see "Money into your account (1 items)"
    And I should see "Money out of your account (1 items)"
    And I should have "£ 300.00" total "income"
    And I should have "£ 100.00" total "expenses"

  Scenario: Admin can generate report, filter by bank account and credit category
    Given I have another bank account
    And I have "300" credit transaction this month
    And I have "100" debit transaction this month
    When I follow "Report"
    And I select "Credit Category" from "category"
    And I select first bank account
    And I press "Filter Result"
    Then I should see "A Summary of Deposit Account"
    And I should see that user bank account
    And I should see my chequing bank account
    And I should see my total opening balance
    And I should see "Money into your account (1 items)"
    And I should not see "Total Income:"
    And I should not see "Total Expenses:"
    And I should not see "£ 100.00"
    And I should have "£ 300.00" for category "Credit Category"

  Scenario: Admin can generate report, filter by bank account and debit category
    Given I have another bank account
    And I have "300" credit transaction this month
    And I have "100" debit transaction this month
    When I follow "Report"
    And I select "Debit Category" from "category"
    And I select first bank account
    And I press "Filter Result"
    Then I should see "A Summary of Deposit Account"
    And I should see that user bank account
    And I should see my chequing bank account
    And I should see my total opening balance
    And I should see "Money out of your account (1 items)"
    And I should not see "Total Income:"
    And I should not see "Total Expenses:"
    And I should not see "£ 300.00"
    And I should have "£ 100.00" for category "Debit Category"

  Scenario: Admin can generate statement
    Given I have "300" credit transaction this month
    And I have "100" debit transaction this month
    When I follow "Statement"
    Then I should see "A Summary of Deposit Account"
    And I should see my chequing bank account
    And I should see my total opening balance
    And I should see "Money into your account (1 items)"
    And I should see "Money out of your account (1 items)"
    And I should see this month closing balance
    And I should not see "Total Income"
    And I should not see "Total Expenses"
    And I should have "credit" transaction item "£ 300.00"
    And I should have "debit" transaction item "£ 100.00"

  Scenario: Admin can generate statement, filter by category
    Given I have "300" credit transaction this month
    And I have "100" debit transaction this month
    When I follow "Statement"
    And I select "Credit Category" from "category"
    And I press "Filter Result"
    Then I should see "A Summary of Deposit Account"
    And I should see my chequing bank account
    And I should see my total opening balance
    And I should see "Money into your account (1 items)"
    And I should not see "Total Income"
    And I should not see "Total Expenses"
    And I should have total "credit" transaction item "£ 300.00", category "Credit Category"
    And I should not see "£ 100.00"

  Scenario: Admin can generate statement, filter by category
    Given I have "300" credit transaction this month
    And I have "100" debit transaction this month
    When I follow "Statement"
    And I select "Debit Category" from "category"
    And I press "Filter Result"
    Then I should see "A Summary of Deposit Account"
    And I should see my chequing bank account
    And I should see my total opening balance
    And I should see "Money out of your account (1 items)"
    And I should not see "Total Income"
    And I should not see "Total Expenses"
    And I should have total "debit" transaction item "£ 100.00", category "Debit Category"
    And I should not see "£ 300.00"

  Scenario: Admin can generate statement, filter by bank account and credit category
    Given I have another bank account
    And I have "300" credit transaction this month
    And I have "100" debit transaction this month
    When I follow "Statement"
    And I select "Credit Category" from "category"
    And I select first bank account
    And I press "Filter Result"
    Then I should see "A Summary of Deposit Account"
    And I should see that user bank account
    And I should see my chequing bank account
    And I should see my total opening balance
    And I should see "Money into your account (1 items)"
    And I should not see "Total Income:"
    And I should not see "Total Expenses:"
    And I should have "£ 300.00" for category "Credit Category"
    And I should not see "£ 100.00"

  Scenario: Admin can generate statement, filter by bank account and debit category
    Given I have another bank account
    And I have "300" credit transaction this month
    And I have "100" debit transaction this month
    When I follow "Statement"
    And I select "Debit Category" from "category"
    And I select first bank account
    And I press "Filter Result"
    Then I should see "A Summary of Deposit Account"
    And I should see that user bank account
    And I should see my chequing bank account
    And I should see my total opening balance
    And I should see "Money out of your account (1 items)"
    And I should not see "Total Income:"
    And I should not see "Total Expenses:"
    And I should have "£ 100.00" for category "Debit Category"
    And I should not see "£ 300.00"