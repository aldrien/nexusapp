Feature: Admins can view and edit user
  In order to manage user
  As an admin
  I want to create and control user
  
  Background: I have an admin logged in
    Given I have admin user
    And that "admin" logs in
    And that "admin" enter valid pin code
    
  Scenario: User List
    Given I have default user
    When I go to the admin manage user page
    Then I should see "Listing All Users"
    And I should see that user name

  Scenario: Edit User Login
    Given I have default user
    When I go to the admin manage user page
    And I edit that User
    And I fill in "Login" with "Joker"
    And I press "Update User"
    Then I should see "User was successfully updated."

  Scenario: Edit User First name
    Given I have default user
    When I go to the admin manage user page
    And I edit that User
    And I fill in "First name" with "Jeff"
    And I press "Update User"
    Then I should see "User was successfully updated."

  Scenario: Edit User Last name
    Given I have default user
    When I go to the admin manage user page
    And I edit that User
    And I fill in "Last name" with "Moreau"
    And I press "Update User"
    Then I should see "User was successfully updated."

  Scenario: Edit User Email
    Given I have default user
    When I go to the admin manage user page
    And I edit that User
    And I fill in "Email" with "banedi@joker.me"
    And I press "Update User"
    Then I should see "User was successfully updated."

  Scenario: Edit User Type
    Given I have default user
    When I go to the admin manage user page
    And I edit that User
    And I select "Supervisor" from "user_user_type_id"
    And I press "Update User"
    Then I should see "User was successfully updated."

  Scenario: Edit User Last name with blank
    Given I have default user
    When I go to the admin manage user page
    And I edit that User
    And I fill in "Last name" with ""
    And I press "Update User"
    Then I should see "Last name can't be blank"
    
  Scenario: Edit User First name with blank
    Given I have default user
    When I go to the admin manage user page
    And I edit that User
    And I fill in "First name" with ""
    And I press "Update User"
    Then I should see "First name can't be blank"
    
  Scenario: Edit User Login with blank
    Given I have default user
    When I go to the admin manage user page
    And I edit that User
    And I fill in "Login" with ""
    And I press "Update User"
    Then I should see "Login can't be blank"
    
  Scenario: Edit User Email with blank
    Given I have default user
    When I go to the admin manage user page
    And I edit that User
    And I fill in "Email" with ""
    And I press "Update User"
    Then I should see "Email can't be blank"

  Scenario: Edit User Invalid Email
    Given I have default user
    When I go to the admin manage user page
    And I edit that User
    And I fill in "Email" with "invalid@email"
    And I press "Update User"
    Then I should see "Email is invalid"
    
  Scenario: Show User
    Given I have default user
    When I go to the admin manage user page
    And I show that User
    And I should see that user name
    
  Scenario: Delete User
    Given I have default user
    When I go to the admin manage user page
    And I follow "Destroy" link of that user
    Then I should see "User was successfully deleted."
