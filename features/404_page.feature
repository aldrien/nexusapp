Feature: User should be redirected to 404 page

  Background: I have an admin logged in
    Given I have admin user
    And that "admin" logs in
    And that "admin" enter valid pin code

  Scenario: User visit no route page
    When I go to the not found page
    Then I should see "404 Error"
    When I follow "Go Back"
    Then I should see "Welcome to Dashboard"