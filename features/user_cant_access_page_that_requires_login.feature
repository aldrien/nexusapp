Feature: User access page that requires login
 
  Background: I have default user
    Given I have default user

  Scenario: Access Set Up 2FA page without loging-in
    When I go to the set up TOTP page
    Then I should see "You need to be logged in to get to that page."

  Scenario: Verify TOTP Code without loging-in
    When I go to the verify TOTP code
    Then I should see "You need to be logged in to get to that page."

  Scenario: User inputs login and password but not TOTP code, then go to dashboard page
    When I go to the user login page
    And I fill in login with the correct "default_user" login
    And I fill in password with the correct "default_user" password
    And I press "Log In"
    When I go to the dashboard page
    Then I should see "You need to be logged in to get to that page."

  Scenario: User inputs login and password but not TOTP code, then go to admin page
    When I go to the user login page
    And I fill in login with the correct "default_user" login
    And I fill in password with the correct "default_user" password
    And I press "Log In"
    When I go to the admin manage user page
    Then I should see "You need to be logged in to get to that page."

  Scenario: User logs in and inactive for few minutes, he should be redirected to login page
    When I go to the user login page
    And I fill in login with the correct "default_user" login
    And I fill in password with the correct "default_user" password
    And I press "Log In"
    Then I should see "Two-Factor Authentication (2FA)"
    When I initialize ROTP
    And I fill in "code" with the generated key
    And I press "Submit"
    Then I should see "You have successfully logged in."
    And I jump forward thirty minutes
    And I follow "Security Settings"
    Then I should see "You need to be logged in to get to that page."

  Scenario: User logs in and inactive for one minute, he should not be redirected to login page
    When I go to the user login page
    And I fill in login with the correct "default_user" login
    And I fill in password with the correct "default_user" password
    And I press "Log In"
    Then I should see "Two-Factor Authentication (2FA)"
    When I initialize ROTP
    And I fill in "code" with the generated key
    And I press "Submit"
    Then I should see "You have successfully logged in."
    And I jump forward one minute
    And I follow "Security Settings"
    Then I should see "Two-Factor Authentication (2FA) Set Up"