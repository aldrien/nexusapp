Feature: Admins can create, view and edit bank account
  In order to manage bank account
  As an admin
  I want to create and control bank accounts
  
  Background: I have an admin logged in
    Given I have admin user
    And that "admin" logs in
    And that "admin" enter valid pin code
    
  Scenario: Bank Account List
    Given I have bank account
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I should see that bank account

  Scenario: Bank Account Show
    Given I have bank account
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I should see that bank account
    And I follow "Show"
    Then I should see that bank account number

  Scenario: New Bank Account
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I follow "New Bank Account"
    And I select "Aldrien Test" from "bank_account_user_id"
    And I fill in "bank_account_description" with "Sample Description"
    And I fill in "bank_account_bank" with "Metro Bank"
    And I select "Savings" from "bank_account_bank_type"
    And I fill in "bank_account_sort_code" with "XS123"
    And I fill in "bank_account_account_number" with "12312412"
    And I fill in "bank_account_opening_balance" with "500.00"
    And I fill in "bank_account_balance" with "300.00"
    And I press "Create Bank account"
    Then I should see "Bank account was successfully created."

  Scenario: New Bank Account, Blank User
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I follow "New Bank Account"
    And I fill in "bank_account_description" with ""
    And I press "Create Bank account"
    Then I should see "Description can't be blank"

  Scenario: New Bank Account, Blank Description
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I follow "New Bank Account"
    And I select "-select here-" from "bank_account_user_id"
    And I press "Create Bank account"
    Then I should see "User can't be blank"

  Scenario: New Bank Account, Blank Bank
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I follow "New Bank Account"
    And I fill in "bank_account_bank" with ""
    And I press "Create Bank account"
    Then I should see "Bank can't be blank"

  # Scenario: New Bank Account, Zero Opening Balance
  #   When I go to the admin bank account page
  #   Then I should see "Listing All Bank Accounts"
  #   And I follow "New Bank Account"
  #   And I fill in "bank_account_opening_balance" with "0.00"
  #   And I press "Create Bank account"
  #   Then I should see "Opening balance must be greater than 0"

  # Scenario: New Bank Account, Blank Opening Balance
  #   When I go to the admin bank account page
  #   Then I should see "Listing All Bank Accounts"
  #   And I follow "New Bank Account"
  #   And I fill in "bank_account_opening_balance" with ""
  #   And I press "Create Bank account"
  #   Then I should see "Opening balance is not a number"

  Scenario: New Bank Account, Blank Bank Type
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I follow "New Bank Account"
    And I press "Create Bank account"
    Then I should see "Bank type can't be blank"

  Scenario: Edit Bank Account, Description
    Given I have bank account
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I should see that bank account
    And I follow "Edit"
    Then I should see "Editing Bank Account"
    And I fill in "bank_account_description" with "New Description"
    And I press "Update Bank account"
    Then I should see "Bank account was successfully updated."
    
  Scenario: Edit Bank Account, Bank
    Given I have bank account
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I should see that bank account
    And I follow "Edit"
    Then I should see "Editing Bank Account"
    And I fill in "bank_account_bank" with "New Bank"
    And I press "Update Bank account"
    Then I should see "Bank account was successfully updated."

  Scenario: Edit Bank Account, Bank Type
    Given I have bank account
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I should see that bank account
    And I follow "Edit"
    Then I should see "Editing Bank Account"
    And I select "Merchant" from "bank_account_bank_type"
    And I press "Update Bank account"
    Then I should see "Bank account was successfully updated."

  Scenario: Edit Bank Account, Sort Code
    Given I have bank account
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I should see that bank account
    And I follow "Edit"
    Then I should see "Editing Bank Account"
    And I fill in "bank_account_sort_code" with "ZZZXXX"
    And I press "Update Bank account"
    Then I should see "Bank account was successfully updated."

  Scenario: Edit Bank Account, Account Number
    Given I have bank account
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I should see that bank account
    And I follow "Edit"
    Then I should see "Editing Bank Account"
    And I fill in "bank_account_account_number" with "0987654321"
    And I press "Update Bank account"
    Then I should see "Bank account was successfully updated."

  Scenario: Edit Bank Account, Balance
    Given I have bank account
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I should see that bank account
    And I follow "Edit"
    Then I should see "Editing Bank Account"
    And I fill in "bank_account_balance" with "500"
    And I press "Update Bank account"
    Then I should see "Bank account was successfully updated."

  Scenario: Edit Bank Account, Blank User
    Given I have bank account
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I should see that bank account
    And I follow "Edit"
    Then I should see "Editing Bank Account"
    And I select "-select here-" from "bank_account_user_id"
    And I press "Update Bank account"
    Then I should see "User can't be blank"

  Scenario: Edit Bank Account, Blank Description
    Given I have bank account
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I should see that bank account
    And I follow "Edit"
    Then I should see "Editing Bank Account"
    And I fill in "bank_account_description" with ""
    And I press "Update Bank account"
    Then I should see "Description can't be blank"

  # Scenario: Edit Bank Account, Blank Opening Balance
  #   Given I have bank account
  #   When I go to the admin bank account page
  #   Then I should see "Listing All Bank Accounts"
  #   And I should see that bank account
  #   And I follow "Edit"
  #   Then I should see "Editing Bank Account"
  #   And I fill in "bank_account_opening_balance" with ""
  #   And I press "Update Bank account"
  #   Then I should see "Opening balance can't be blank"

  # Scenario: Edit Bank Account, Zero Opening Balance
  #   Given I have bank account
  #   When I go to the admin bank account page
  #   Then I should see "Listing All Bank Accounts"
  #   And I should see that bank account
  #   And I follow "Edit"
  #   Then I should see "Editing Bank Account"
  #   And I fill in "bank_account_opening_balance" with "0.00"
  #   And I press "Update Bank account"
  #   Then I should see "Opening balance must be greater than 0"
    
  Scenario: Edit Bank Account, Blank Bank
    Given I have bank account
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I should see that bank account
    And I follow "Edit"
    Then I should see "Editing Bank Account"
    And I fill in "bank_account_bank" with ""
    And I press "Update Bank account"
    Then I should see "Bank can't be blank"

  Scenario: Edit Bank Account, Blank Bank Type
    Given I have bank account
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I should see that bank account
    And I follow "Edit"
    Then I should see "Editing Bank Account"
    And I select "select here" from "bank_account_bank_type"
    And I press "Update Bank account"
    Then I should see "Bank type can't be blank"

  Scenario: Admin Re-Calculate Account Balance
    Given I have bank account
    And I have credit transaction
    And I have debit transaction
    When I go to the admin bank account page
    Then I should see "Listing All Bank Accounts"
    And I should see that bank account
    And I follow "Recalculate"
    Then I should see updated balance