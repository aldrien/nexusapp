Feature: Admins can create, view and edit Transaction
  In order to manage Transaction
  As an admin
  I want to create and control Transactions
  
  Background: I have an admin logged in
    Given I have admin user
    And that "admin" logs in
    And that "admin" enter valid pin code
    
  Scenario: Transaction List
    Given I have transaction
    When I go to the admin manage transaction page
    Then I should see "Listing All Transactions"
    And I should see that transaction

  Scenario: Transaction Show
    Given I have transaction
    When I go to the admin manage transaction page
    Then I should see "Listing All Transactions"
    And I should see that transaction
    And I follow "Show"
    Then I should see that transaction amount

  Scenario: New Transaction, Bank Account Balance Increase
    Given I have credit transaction category
    And I have bank account
    And I load bank acount balance into variable
    When I go to the admin manage transaction page
    Then I should see "Listing All Transactions"
    And I follow "New Transaction"
    And I select default user from "transaction_account_id"
    And I fill in "transaction_amount" with "50"
    And I fill in "transaction_description" with "Sample Description"
    And I select "Credit Category" from "transaction_transaction_category_id"
    And I select "2017" from "transaction_transaction_date_1i"
    And I select "February" from "transaction_transaction_date_2i"
    And I select "14" from "transaction_transaction_date_3i"
    And I select "10" from "transaction_transaction_date_4i"
    And I select "30" from "transaction_transaction_date_5i"
    And I press "Create Transaction"
    Then I should see "Transaction was successfully created."
    And bank acount balance should "decrease" by "50"
    And that transaction credit should be "true"

  Scenario: New Transaction, Bank Account Balance Decrease
    Given I have credit and debit transaction category
    And I have bank account
    And I load bank acount balance into variable
    When I go to the admin manage transaction page
    Then I should see "Listing All Transactions"
    And I follow "New Transaction"
    And I select default user from "transaction_account_id"
    And I fill in "transaction_amount" with "50"
    And I fill in "transaction_description" with "Sample Description"
    And I select "Credit Category" from "transaction_transaction_category_id"
    And I select "2017" from "transaction_transaction_date_1i"
    And I select "February" from "transaction_transaction_date_2i"
    And I select "14" from "transaction_transaction_date_3i"
    And I select "10" from "transaction_transaction_date_4i"
    And I select "30" from "transaction_transaction_date_5i"
    And I press "Create Transaction"
    Then I should see "Transaction was successfully created."
    And bank acount balance should "increase" by "50"
    And that transaction credit should be "false"
    
  Scenario: New Transaction, Blank Account Id
    When I go to the admin manage transaction page
    Then I should see "Listing All Transactions"
    And I follow "New Transaction"
    And I select "-select here-" from "transaction_account_id"
    And I press "Create Transaction"
    Then I should see "Account can't be blank"

  Scenario: New Transaction, Blank Amount
    When I go to the admin manage transaction page
    Then I should see "Listing All Transactions"
    And I follow "New Transaction"
    And I fill in "transaction_amount" with ""
    And I press "Create Transaction"
    Then I should see "Amount can't be blank"

  Scenario: New Transaction, Blank Description
    When I go to the admin manage transaction page
    Then I should see "Listing All Transactions"
    And I follow "New Transaction"
    And I fill in "transaction_description" with ""
    And I press "Create Transaction"
    Then I should see "Description can't be blank"

  Scenario: New Transaction, Blank Category
    When I go to the admin manage transaction page
    Then I should see "Listing All Transactions"
    And I follow "New Transaction"
    And I select "-select here-" from "transaction_transaction_category_id"
    And I press "Create Transaction"
    Then I should see "Transaction category can't be blank"

  Scenario: Edit Transaction, Amount
    Given I have transaction
    When I go to the admin manage transaction page
    Then I should see "Listing All Transactions"
    And I should see that transaction
    And I follow "Edit"
    Then I should see "Editing Transaction"
    And I fill in "transaction_amount" with "500"
    And I press "Update Transaction"
    Then I should see "Transaction was successfully updated."

  Scenario: Edit Transaction, Description
    Given I have transaction
    When I go to the admin manage transaction page
    Then I should see "Listing All Transactions"
    And I should see that transaction
    And I follow "Edit"
    Then I should see "Editing Transaction"
    And I fill in "transaction_description" with "New Description"
    And I press "Update Transaction"
    Then I should see "Transaction was successfully updated."

  Scenario: Edit Transaction, Category
    Given I have transaction
    And I have debit transaction category
    When I go to the admin manage transaction page
    Then I should see "Listing All Transactions"
    And I should see that transaction
    And I follow "Edit"
    Then I should see "Editing Transaction"
    And I select "Credit Category" from "transaction_transaction_category_id"
    And I press "Update Transaction"
    Then I should see "Transaction was successfully updated."
    And that transaction credit should be "false"

  Scenario: Edit Transaction, Date
    Given I have transaction
    When I go to the admin manage transaction page
    Then I should see "Listing All Transactions"
    And I should see that transaction
    And I follow "Edit"
    Then I should see "Editing Transaction"
    And I select "2017" from "transaction_transaction_date_1i"
    And I select "March" from "transaction_transaction_date_2i"
    And I select "20" from "transaction_transaction_date_3i"
    And I select "11" from "transaction_transaction_date_4i"
    And I select "45" from "transaction_transaction_date_5i"
    And I press "Update Transaction"
    Then I should see "Transaction was successfully updated."

  Scenario: Edit Transaction, Blank Bank Account
    Given I have transaction
    When I go to the admin manage transaction page
    Then I should see "Listing All Transactions"
    And I should see that transaction
    And I follow "Edit"
    Then I should see "Editing Transaction"
    And I select "-select here-" from "transaction_account_id"
    And I press "Update Transaction"
    Then I should see "Bank account must exist"

  Scenario: Edit Transaction, Blank Description
    Given I have transaction
    When I go to the admin manage transaction page
    Then I should see "Listing All Transactions"
    And I should see that transaction
    And I follow "Edit"
    Then I should see "Editing Transaction"
    And I fill in "transaction_description" with ""
    And I press "Update Transaction"
    Then I should see "Description can't be blank"
    
  Scenario: Edit Transaction, Blank Amount
    Given I have transaction
    When I go to the admin manage transaction page
    Then I should see "Listing All Transactions"
    And I should see that transaction
    And I follow "Edit"
    Then I should see "Editing Transaction"
    And I fill in "transaction_amount" with ""
    And I press "Update Transaction"
    Then I should see "Amount can't be blank"

  Scenario: Edit Transaction, Blank Category
    Given I have transaction
    When I go to the admin manage transaction page
    Then I should see "Listing All Transactions"
    And I should see that transaction
    And I follow "Edit"
    Then I should see "Editing Transaction"
    And I select "-select here-" from "transaction_transaction_category_id"
    And I press "Update Transaction"
    Then I should see "Transaction category can't be blank"