Feature: Admins can create, view and edit Transaction Catergory
  In order to manage Transaction
  As an admin
  I want to create and control Transaction Categories
  
  Background: I have an admin logged in
    Given I have admin user
    And that "admin" logs in
    And that "admin" enter valid pin code
    
  Scenario: Transaction Catergory List
    Given I have credit transaction category
    When I go to the admin manage transaction category page
    Then I should see "Listing All Transaction Categories"
    And I should see that transaction category

  Scenario: New Transaction Catergory
    When I go to the admin manage transaction category page
    Then I should see "Listing All Transaction Categories"
    And I follow "New Transaction"
    And I fill in "transaction_category_title" with "Sample"
    And I fill in "transaction_category_description" with "Sample Description"
    When I check "transaction_category_credit"
    And I press "Create Transaction category"
    Then I should see "Transaction category was successfully created."

  Scenario: New Transaction Catergory, Blank Title
    When I go to the admin manage transaction category page
    Then I should see "Listing All Transaction Categories"
    And I follow "New Transaction"
    And I fill in "transaction_category_title" with ""
    And I press "Create Transaction category"
    Then I should see "Title can't be blank"

  Scenario: New Transaction Catergory, Blank Description
    When I go to the admin manage transaction category page
    Then I should see "Listing All Transaction Categories"
    And I follow "New Transaction"
    And I fill in "transaction_category_description" with ""
    And I press "Create Transaction category"
    Then I should see "Description can't be blank"

  Scenario: Edit Transaction Catergory, Title
    Given I have credit transaction category
    When I go to the admin manage transaction category page
    Then I should see "Listing All Transaction Categories"
    And I should see that transaction category
    And I follow "Edit"
    Then I should see "Editing Transaction Category"
    And I fill in "transaction_category_title" with "New title"
    And I press "Update Transaction category"
    Then I should see "Transaction category was successfully updated."

  Scenario: Edit Transaction Catergory, Description
    Given I have credit transaction category
    When I go to the admin manage transaction category page
    Then I should see "Listing All Transaction Categories"
    And I should see that transaction category
    And I follow "Edit"
    Then I should see "Editing Transaction Category"
    And I fill in "transaction_category_description" with "New Description"
    And I press "Update Transaction category"
    Then I should see "Transaction category was successfully updated."

  Scenario: Edit Transaction Catergory, Blank Description
    Given I have credit transaction category
    When I go to the admin manage transaction category page
    Then I should see "Listing All Transaction Categories"
    And I should see that transaction category
    And I follow "Edit"
    Then I should see "Editing Transaction Category"
    And I fill in "transaction_category_description" with ""
    And I press "Update Transaction category"
    Then I should see "Description can't be blank"
    
  Scenario: Edit Transaction Catergory, Blank Title
    Given I have credit transaction category
    When I go to the admin manage transaction category page
    Then I should see "Listing All Transaction Categories"
    And I should see that transaction category
    And I follow "Edit"
    Then I should see "Editing Transaction Category"
    And I fill in "transaction_category_title" with ""
    And I press "Update Transaction category"
    Then I should see "Title can't be blank"

  Scenario: Delete Transaction Catergory
    Given I have credit transaction category
    When I go to the admin manage transaction category page
    And I follow "Destroy" link of that transaction category
    Then I should see "Transaction category was successfully deleted."