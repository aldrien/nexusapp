Feature: Users can reset their password
  As a user
  I want to be able to reset my passowrd
  
  Background:
    Given I have default user
    
  Scenario: Reset Password
    When I go to the user login page
    And I follow "Forgot password?"
    And I fill in "email" with default_user email
    And I press "Reset Password"
    Then I should see "Email sent with password reset instructions."
    And that default_user opens the email with subject "Password reset"
    And I click the first link in the email
    Then I should see "Reset Your Password"
    And I fill in "user_password" with "new_password"
    And I fill in "user_password_confirmation" with "new_password"
    And I press "Update Password"
    Then I should see "Password has been reset!"
    
  Scenario: Reset Password, missmatched passwords
    When I go to the user login page
    And I follow "Forgot password?"
    And I fill in "email" with default_user email
    And I press "Reset Password"
    Then I should see "Email sent with password reset instructions."
    And that default_user opens the email with subject "Password reset"
    And I click the first link in the email
    Then I should see "Reset Your Password"
    And I fill in "user_password" with "new_password"
    And I fill in "user_password_confirmation" with "old_password"
    And I press "Update Password"
    Then I should see "Password and confimation not matched!"
    
  Scenario: Reset Password, invalid email address
    When I go to the user login page
    And I follow "Forgot password?"
    And I fill in "email" with "invalid@gmail.com"
    And I press "Reset Password"
    Then I should see "Email address not recognised."
    
  Scenario: Reset password, token expired
    When I time travel to December 21 2015, 19:00
    And I go to the user login page
    And I follow "Forgot password?"
    And I fill in "email" with default_user email
    And I press "Reset Password"
    Then I should see "Email sent with password reset instructions."
    And I time travel to December 23 2015, 19:00
    And that default_user opens the email with subject "Password reset"
    And I click the first link in the email
    Then I should see "Password reset token has expired"