Feature: Check PDF file contents

  Background: I have PDF Report
    Given I have admin user
    And that "admin" logs in
    And that "admin" enter valid pin code
    And I have bank account
    And I have "300" credit transaction this month
    And I have "100" debit transaction this month

  Scenario: Check printable Report PDF
    When I follow "Report"
    Then I should see "A Summary of Deposit Account" 
    And I follow the PDF link "Generate Report"
    And I should get sucess status
    And I should get "application/pdf" content type
    And I should get proper pdf "Report" file name
    And I should see account holder name
    And I should see one "credit" transaction
    And I should see one "debit" transaction
    And I should see chequing bank account
    And I should see total opening balance
    And I should see month closing balance
    And I should see total income
    And I should see total expenses

  Scenario: Check printable Report PDF, filter by credit category
    When I follow "Report"
    And I select "Credit Category" from "category"
    And I press "Filter Result"
    Then I should see "A Summary of Deposit Account" 
    And I follow the PDF link "Generate Report"
    And I should get sucess status
    And I should get "application/pdf" content type
    And I should get proper pdf "Report" file name
    And I should see account holder name
    And I should see one "credit" transaction
    And I should not see one "debit" transaction
    And I should see chequing bank account
    And I should see total opening balance
    And I should see month closing balance
    And I should not see total income
    And I should not see total expenses

  Scenario: Check printable Report PDF, filter by debit category
    When I follow "Report"
    And I select "Debit Category" from "category"
    And I press "Filter Result"
    Then I should see "A Summary of Deposit Account" 
    And I follow the PDF link "Generate Report"
    And I should get sucess status
    And I should get "application/pdf" content type
    And I should get proper pdf "Report" file name
    And I should see account holder name
    And I should not see one "credit" transaction
    And I should see one "debit" transaction
    And I should see chequing bank account
    And I should see total opening balance
    And I should see month closing balance
    And I should not see total income
    And I should not see total expenses

  Scenario: Check printable Statement PDF
    When I follow "Statement"
    Then I should see "A Summary of Deposit Account" 
    And I follow the PDF link "Generate Report"
    And I should get sucess status
    And I should get "application/pdf" content type
    And I should get proper pdf "Statement" file name
    And I should see account holder name
    And I should see one "credit" transaction
    And I should see one "debit" transaction
    And I should see chequing bank account
    And I should see total opening balance
    And I should see month closing balance
    And I should not see total income
    And I should not see total expenses