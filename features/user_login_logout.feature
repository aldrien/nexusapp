Feature: Users can log in and out

  Background: I have admin user
    Given I have admin user

  Scenario: Sucessfully log in
    When I go to the user login page
    And I fill in login with the correct "admin" login
    And I fill in password with the correct "admin" password
    And I press "Log In"
    Then I should see "Two-Factor Authentication (2FA)"
    When I initialize ROTP
    And I fill in "code" with the generated key
    And I press "Submit"
    Then I should see "You have successfully logged in."
    
  Scenario: Invalid log in
    When I go to the user login page
    And I fill in "login" with "Sample"
    And I fill in "Password" with "Foo"
    And I press "Log In"
    Then I should see "Your username or password were not recognised."

 Scenario: Invalid authentication code
    When I go to the user login page
    And I fill in login with the correct "admin" login
    And I fill in password with the correct "admin" password
    And I press "Log In"
    Then I should see "Two-Factor Authentication (2FA)"
    When I initialize ROTP
    And I fill in "code" with "123456"
    And I press "Submit"
    Then I should see "Invalid code. Please try again!"

  Scenario: Log out
    When I go to the user login page
    And I fill in login with the correct "admin" login
    And I fill in password with the correct "admin" password
    And I press "Log In"
    Then I should see "Two-Factor Authentication (2FA)"
    When I initialize ROTP
    And I fill in "code" with the generated key
    And I press "Submit"
    Then I should see "You have successfully logged in."
    And I follow "Log Out"
    Then I should see "You have successfully logged out."

  Scenario: User tries to login with disabled 2FA
    Given I have user with deactivated TOTP
    When I go to the user login page
    And I fill in login with the correct "default_user" login
    And I fill in password with the correct "default_user" password
    And I press "Log In"
    Then I should see "You have successfully logged in."

  Scenario: User uses his TOTP Recovery Code to login
    When I go to the user login page
    And I fill in login with the correct "admin" login
    And I fill in password with the correct "admin" password
    And I press "Log In"
    Then I should see "Two-Factor Authentication (2FA)"
    And I follow "Login using Backup Code"
    When I initialize ROTP
    And I fill in "backup_code" with the user recovery code
    And I press "Submit"
    Then I should see "Login via Backup Code was successful."
    And I follow "Continue >>"
    Then I should see "You have successfully logged in."

  Scenario: User uses his TOTP Recovery Code to login, Invalid
    When I go to the user login page
    And I fill in login with the correct "admin" login
    And I fill in password with the correct "admin" password
    And I press "Log In"
    Then I should see "Two-Factor Authentication (2FA)"
    And I follow "Login using Backup Code"
    When I initialize ROTP
    And I fill in "backup_code" with "asdasdafdfsd1231"
    And I press "Submit"
    Then I should see "Invalid code. Please try again!"