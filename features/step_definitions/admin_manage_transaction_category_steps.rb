Given(/^I have credit transaction category$/) do
  @credit_transaction_category = FactoryGirl.create(:credit_transaction_category)
end

Given(/^I have debit transaction category$/) do
  @debit_transaction_category = FactoryGirl.create(:debit_transaction_category)
end

Given(/^I have credit and debit transaction category$/) do
  @credit_transaction_category = FactoryGirl.create(:credit_transaction_category)
  @debit_transaction_category = FactoryGirl.create(:debit_transaction_category)
end

Then(/^I should see that transaction category$/) do
  assert page.has_content?(@credit_transaction_category.title)
end

When(/^I follow "([^"]*)" link of that transaction category$/) do |text|
  find(:xpath, "//a[@href='/admin/transaction_categories/#{@credit_transaction_category.id}']", text: text).click
end