Given(/^I have transaction$/) do
  @transaction = FactoryGirl.create(:bank_transaction)
end

Then(/^I should see that transaction$/) do
  assert page.has_content?(@transaction.user.fullname)
end

Then(/^I should see that transaction amount$/) do
  assert page.has_content?(@transaction.amount)
end

When(/^I fill in "([^"]*)" with current dateTime$/) do |field|
  fill_in(field, :with => Time.now)
end

Given(/^I load bank acount balance into variable$/) do
  @previous_balance = @bank_account.balance
end

Then(/^bank acount balance should "([^"]*)" by "([^"]*)"$/) do |method, amount|
  # current balance is 500, see test/factories/bank_account
  @bank_account.reload
  if method == "increase"
    @bank_account.balance.to_f == (@previous_balance.to_f + Transaction.last.amount.to_f)
  else
    @bank_account.balance.to_f == (@previous_balance.to_f - Transaction.last.amount.to_f)
  end
end

Then(/^I select default user from "([^"]*)"$/) do |field|
  select("#{@bank_account.user.fullname} - #{@bank_account.account_number}", :from => field)
end

Then(/^that transaction credit should be "([^"]*)"$/) do |is_credit|
  newly_created_transaction = Transaction.last
  if is_credit == "true"
   newly_created_transaction.credit == true
  else
    newly_created_transaction.credit == false
  end
end