When(/^I edit that User$/) do
  visit edit_admin_user_path(@default_user)
end

When(/^I show that User$/) do
  visit admin_user_path(@default_user)
end

When(/^I follow "([^"]*)" link of that user$/) do |text|
  find(:xpath, "//a[@href='/admin/users/#{@default_user.id}']", text: text).click
end