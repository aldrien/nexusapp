When /^I go to (.*)$/ do |page_name|
  visit path_to(page_name)
end

Then /^I should see "(.*?)"$/ do |text|
  if page.respond_to? :should
    page.should have_content(text)
  else
    assert page.has_content?(text)
  end
end

Then /^I should not see "(.*?)"$/ do |text|
  assert page.has_no_content?(text)
end

When /^I follow "([^\"]*)"$/ do |link|
  click_link(link)
end

When /^I fill in "([^"]*)" with "([^"]*)"$/ do |field, value|
  fill_in(field, :with => value)
end

When /^I press "([^"]*)"$/ do |button|
  click_button(button)
end

Then /^show me the page$/ do
 save_and_open_page
end

Then(/^show me a screenshot$/) do
  file_path = "/tmp/screenshot.png"
  page.save_screenshot file_path
  Launchy.open file_path
end

When /^I select "([^"]*)" from "([^"]*)"$/ do |value, field|
  select(value, :from => field)
end

When /^I check "([^\"]*)"$/ do |field|
  check(field)
end

When /^I uncheck "([^\"]*)"$/ do |field|
  uncheck(field)
end

When(/^I choose "([^"]*)"$/) do |radio|
  choose(radio)
end

When /^I confirm popup$/ do
  page.driver.browser.switch_to.alert.accept    
end

When /^I dismiss popup$/ do
  page.driver.browser.switch_to.alert.dismiss
end

When /^I wait for (\d+) seconds?$/ do |secs|
  sleep secs.to_i
end