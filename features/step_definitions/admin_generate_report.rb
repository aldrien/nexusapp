Given(/^I have "([^"]*)" credit transaction this month$/) do |amount|
  @transaction = FactoryGirl.create(:new_credit_transaction, {
    account_id: @bank_account.id,
    amount: amount
  })
end

Given(/^I have "([^"]*)" debit transaction this month$/) do |amount|
  @transaction = FactoryGirl.create(:new_debit_transaction, {
    account_id: @bank_account.id,
    amount: amount
  })
end

Given(/^I have "([^"]*)" credit transaction last month$/) do |amount|
  @transaction = FactoryGirl.create(:new_credit_transaction, {
    account_id: @bank_account.id,
    amount: amount,
    credit: true,
    transaction_date: Time.now.months_since(-1)
  })
end

Given(/^I have "([^"]*)" debit transaction last month$/) do |amount|
  @transaction = FactoryGirl.create(:new_debit_transaction, {
    account_id: @bank_account.id,
    credit: false,
    amount: amount,
    transaction_date: Time.now.months_since(-1)
  })
end

When(/^I select previous month$/) do
  select((Time.now.months_since(-1)).strftime("%B"), :from => 'month')
end

Then(/^I should see my chequing bank account$/) do
  assert page.has_content?("Unlimited chequing Account ##{@bank_account.account_number}")
end

Then(/^I should see my total opening balance$/) do
  assert page.has_content?("Your account opening balance")
  assert page.has_content?(number_to_currency_gbp(@bank_account.opening_balance))
end

Then(/^I should see this month closing balance$/) do
  credit_amount = @bank_account.bank_transactions.records_of_this_month.get_total_credit.sum(:amount)
  debit_amount = @bank_account.bank_transactions.records_of_this_month.get_total_debit.sum(:amount)

  @closing_balance = (@bank_account.opening_balance.to_f + credit_amount.to_f) - debit_amount.to_f
  assert page.has_content?(number_to_currency_gbp(@closing_balance))
end

Then(/^I should see account balance forward$/) do
  month = (Time.now.months_since(-1)).strftime("%m").to_i

  forward_balance_text = "Your account balance forward on #{get_last_month_and_last_day(month).strftime("%B %d, %Y")}"
  assert page.has_content?(forward_balance_text)

  forward_balance_amount = number_to_currency_gbp(get_last_month_forward_balance(@bank_account, month))
  find(:xpath, ".//td[contains(., '#{forward_balance_text}')]/following-sibling::td[1]//*[text()='#{forward_balance_amount}']")
end

Then(/^I should see last month closing balance$/) do
  last_month = (Time.now.months_since(-1)).strftime("%m").to_i
  @last_month_closing_balance = @bank_account.bank_transactions.records_by_month(last_month)
  credit_balance = @bank_account.bank_transactions.records_by_month(last_month).get_total_credit.sum(:amount)
  debit_balance = @bank_account.bank_transactions.records_by_month(last_month).get_total_debit.sum(:amount)
  this_month_opening_balance = number_to_currency_gbp(@bank_account.opening_balance.to_f + credit_balance.to_f - debit_balance.to_f)

  closing_balance_text = "Your closing balance on #{get_closing_balance_date(last_month)}"
  assert page.has_content?(closing_balance_text)
  find(:xpath, ".//td[contains(., '#{closing_balance_text}')]/following-sibling::td[2]//*[text()='#{this_month_opening_balance}']")
end

Then(/^I should have "([^"]*)" total "([^"]*)"$/) do |amount, type|
  if type == 'income' 
    find(:xpath, ".//td[contains(., 'Total Income:')]/following-sibling::td[2]//*[text()='#{amount}']")
  elsif type == 'expenses'
    find(:xpath, ".//td[contains(., 'Total Expenses:')]/following-sibling::td//*[text()='#{amount}']")
  end
end

Then(/^I should have "([^"]*)" for category "([^"]*)"$/) do |amount, category|
  find(:xpath, "//table[@id='transaction_table']//tr[2]//td[contains(., '#{category}')]/following-sibling::td[contains(., '#{amount}')]")
end

Given(/^I have another bank account$/) do
  @another_bank_account = FactoryGirl.create(:bank_account, account_number: rand(1_000_000_000..9_999_999_999))
end

When(/^I select first bank account$/) do
  select("#{@bank_account.user.fullname} - #{@bank_account.account_number}", :from => 'bank_account')
end

Then(/^I should see that user bank account$/) do
  assert page.has_content?("#{@bank_account.user.fullname} - #{@bank_account.account_number}")
end

Then(/^I should have "([^"]*)" transaction item "([^"]*)"$/) do |type, amount|
  first_transaction = @bank_account.bank_transactions.first
  last_transaction = @bank_account.bank_transactions.last
  if type == 'credit'
    find(:xpath, "//table[@id='transaction_table']//tr[2]//td[contains(., '#{first_transaction.transaction_date.strftime("%b %e")}')]/following-sibling::td[contains(., '#{amount}')]")
  elsif type == 'debit'
    find(:xpath, "//table[@id='transaction_table']//tr[3]//td[contains(., '#{last_transaction.transaction_date.strftime("%b %e")}')]/following-sibling::td[contains(., '#{amount}')]")
  end
end

Then(/^I should have total "([^"]*)" transaction item "([^"]*)", category "([^"]*)"$/) do |type, amount, category|
  first_transaction = @bank_account.bank_transactions.first
  last_transaction = @bank_account.bank_transactions.last
  if type == 'credit'
    find(:xpath, "//table[@id='transaction_table']//tr[2]//td[contains(., '#{category}')]/following-sibling::td[contains(., '#{amount}')]")
  elsif type == 'debit'
    find(:xpath, "//table[@id='transaction_table']//tr[2]//td[contains(., '#{category}')]/following-sibling::td[contains(., '#{amount}')]")
  end
end