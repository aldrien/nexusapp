When(/^I initialize ROTP$/) do
  @current_user = @admin || @user || @default_user
  @current_user.totp_secret = ROTP::Base32.random_base32
  @current_user.totp_recovery_code = encrypt(ROTP::Base32.random_base32)
  @current_user.save

  @rotp = RubyOneTimePassword.new(@current_user.totp_secret, @current_user.email)
end

When(/^I fill in "([^"]*)" with the generated key$/) do |field|
  fill_in(field, :with => @rotp.get_now)
end
