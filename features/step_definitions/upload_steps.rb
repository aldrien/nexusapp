Then(/^I upload new image$/) do
  @default_file = Rails.root.join('features', 'upload_files', 'sample-profile.png')  
  attach_file('user[photo]', @default_file)
end

Then(/^I should have new user photo$/) do
  @default_user.photo.blank? == false
  @default_user.photo == 'sample-profile.png'
end

Then(/^I should see the new photo in page$/) do
  file_url = @default_user.photo_url(:medium)
  assert page.has_xpath?("//img[@src=\"#{file_url}\"]")
end

Then(/^I upload invalid file type$/) do
  @default_file = Rails.root.join('features', 'upload_files', 'test-invalid-file.csv')  
  attach_file('user[photo]', @default_file)
end