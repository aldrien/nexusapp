Given /^I time travel to (.+)$/ do |period|
  Delorean.time_travel_to(period)
end

Given /^I jump forward one minute$/ do
  Delorean.jump 60 # by seconds
end

Given /^I jump forward thirty minutes$/ do
  Delorean.jump 1800 # by seconds
end

Given /^I go back to the present$/ do
  Delorean.back_to_the_present
end