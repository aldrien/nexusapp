Given(/^I have bank account$/) do
  @bank_account = FactoryGirl.create(:bank_account, account_number: rand(1_000_000_000..9_999_999_999))
end

Then(/^I should see that bank account$/) do
  assert page.has_content?(@bank_account.bank_type)
end

Then(/^I should see that bank account number$/) do
  assert page.has_content?(@bank_account.account_number)
end

Given(/^I have credit transaction$/) do
  @credit_transaction = FactoryGirl.create(:new_credit_transaction, account_id: @bank_account.id)
end

Given(/^I have debit transaction$/) do
  @debit_transaction = FactoryGirl.create(:new_debit_transaction, account_id: @bank_account.id)
end

Then(/^I follow "([^"]*)" with current bank account$/) do |text|
  find(:xpath, "//a[@data-bank-account-id='#{@bank_account.id}']").click
end

Then(/^I should see updated balance$/) do
  new_balance = (@bank_account.opening_balance + @bank_account.bank_transactions.get_total_credit.sum(:amount)) - @bank_account.bank_transactions.get_total_debit.sum(:amount)
  assert page.has_content?(number_to_currency_gbp(new_balance))
end