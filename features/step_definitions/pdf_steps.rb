require 'tempfile'

Then(/^I follow the PDF link "([^"]*)"$/) do |text|
  find("a[title='#{text}']").click
  temp_pdf = Tempfile.new('pdf')
  if Capybara.current_driver == Capybara.javascript_driver
    temp_pdf << page.driver.source.force_encoding('UTF-8')
  else
    temp_pdf << page.driver.response.body.force_encoding('UTF-8')
  end
  temp_pdf.close

  temp_txt = Tempfile.new('txt')
  temp_txt.close
  `pdftotext -q #{temp_pdf.path} #{temp_txt.path}`
  @body = File.read temp_txt.path
  @body.gsub!("\f", "\n")
end

Then(/^I should get sucess status$/) do
  assert_equal(page.status_code, 200)
end

Then(/^I should get "([^"]*)" content type$/) do |arg1|
  page.driver.response.headers['Content-Type'] == "application/pdf"
end

Then(/^I should get proper pdf "([^"]*)" file name$/) do |title|
  content_disposition = page.driver.response.headers['Content-Disposition']
  assert_equal(content_disposition, "inline; filename=\"#{title}| #{@bank_account.user.fullname}.pdf\"")
end

Then(/^I should see account holder name$/) do
  @body.include?("#{@bank_account.user.fullname} - #{@bank_account.account_number}")
end

Then(/^I should see one "([^"]*)" transaction$/) do |type|
  case type
  when 'credit'
    @body.include?("Money into your account (1 items)")
  when 'debit'
    @body.include?("Money out of your account (1 items)")
  end
end

Then(/^I should not see one "([^"]*)" transaction$/) do |type|
  case type
  when 'credit'
    !@body.include?("Money into your account (1 items)")
  when 'debit'
    !@body.include?("Money out of your account (1 items)")
  end
end

Then(/^I should see chequing bank account$/) do
  @body.include?("A Summary of Deposit Account")
  @body.include?("Unlimited chequing Account ##{@bank_account.account_number}")
end

Then(/^I should see total opening balance$/) do
  @body.include?("Your account opening balance")
  @body.include?(number_to_currency_gbp(@bank_account.opening_balance))
end

Then(/^I should see month closing balance$/) do
  credit_amount = @bank_account.bank_transactions.records_of_this_month.get_total_credit.sum(:amount)
  debit_amount = @bank_account.bank_transactions.records_of_this_month.get_total_debit.sum(:amount)

  @closing_balance = (@bank_account.opening_balance.to_f + credit_amount.to_f) - debit_amount.to_f
  @body.include?(number_to_currency_gbp(@closing_balance))
end

Then(/^I should see total income$/) do
  @body.include?("Total Income:")
end

Then(/^I should see total expenses$/) do
  @body.include?("Total Expenses:")
end

Then(/^I should not see total income$/) do
  !@body.include?("Total Income:")
end

Then(/^I should not see total expenses$/) do
  !@body.include?("Total Expenses:")
end

# assert_no_text("")
# assert_not_equal(expectation, actual)