Given(/^I have admin user$/) do
  @admin = FactoryGirl.create(:admin)
  @admin.save!
end

Given(/^I have default user$/) do
  @default_user = FactoryGirl.create(:default_user)
  @default_user.save!
end

Given(/^I have user with deactivated TOTP$/) do
  @default_user = FactoryGirl.create(:default_user, {activate_totp: false})

end

Given(/^that "([^"]*)" logs in$/) do |user_type|
  if user_type == 'admin'
    @set_login = @admin.login
    @set_password = @admin.password
  else
    @set_login = @default_user.login
    @set_password = @default_user.password
  end

  visit login_path
  fill_in "login", :with => @set_login
  fill_in "password", :with => @set_password
  click_button "Log In"
end

When(/^I fill in login with the correct "([^"]*)" login$/) do |user_type|
  if user_type == 'admin'
    @set_login = @admin.login
  elsif user_type == "default_user"
    @set_login = @default_user.login
  elsif user_type == "last_user"
    @set_login = User.last.login
  end

  fill_in "login", :with => @set_login
end

Given(/^that "([^"]*)" enter valid pin code$/) do |user_type|
  if user_type == 'admin'
    @set_ropt_secret = @admin.totp_secret
    @set_email = @admin.email
  else
    @set_ropt_secret = @default_user.totp_secret
    @set_email = @default_user.email
  end

  @rotp = RubyOneTimePassword.new(@set_ropt_secret, @set_email)
  fill_in('code', :with => @rotp.get_now)
  click_button "Submit"
end

When(/^I fill in password with the correct "([^"]*)" password$/) do |user_type|
  if user_type == 'admin'
    @set_password = @admin.password
  elsif user_type == "default_user"
    @set_password = @default_user.password
  elsif user_type == "last_user"
    @set_password = User.last.password
  end
  
  fill_in "password", :with => @set_password
end

Then(/^I load that new user to a variable$/) do
  @user = User.last
end

Then(/^I should see that user name$/) do
  assert page.has_content?(@default_user.first_name)
end

Then(/^I should see user TOTP recovery code$/) do
  assert page.has_content?(decrypt(User.last.totp_recovery_code))
end

Then(/^I fill in "([^"]*)" with the user recovery code$/) do |textbox|
  fill_in textbox, :with => decrypt(@admin.totp_recovery_code)
end

Then(/^I fill in "([^"]*)" with "([^"]*)" old password$/) do |field, user_type|
  if user_type == 'admin'
    @set_old_password = @admin.password
  else
    @set_old_password = @default_user.password
  end
  
  fill_in "old_password", :with => @set_old_password
end

Then(/^I fill in "([^"]*)" with "([^"]*)" valid password$/) do |field, user_type|
  if user_type == 'admin'
    @set_password = @admin.password
  else
    @set_password = @default_user.password
  end
  
  fill_in field, :with => @set_password
end


When /^I fill in "([^"]*)" with default_user email$/ do |field|
  fill_in(field, :with => @default_user.email)
end

When(/^I fill in "([^"]*)" with default user login$/)  do |field|
  fill_in field, :with => @default_user.login
end

When(/^I fill in "([^"]*)" with default user email$/) do |field|
  fill_in field, :with => @default_user.email
end