class RubyOneTimePassword
  require 'rqrcode'

  attr_reader :totp_secret, :user_email

  def initialize(totp_secret, user_email)
    @totp_secret = totp_secret
    @user_email = user_email

    @totp = ROTP::TOTP.new(@totp_secret, issuer: "Nexus")
    @qrcode = RQRCode::QRCode.new(@totp.provisioning_uri(@user_email))
  end

  def generate_qr
    @qrcode.as_png(
                resize_gte_to: false,
                resize_exactly_to: false,
                fill: 'white',
                color: 'black',
                size: 175,
                border_modules: 4,
                module_px_size: 6,
                file: nil # path to write
                )
  end

  def verify(code)
    @totp.verify_with_drift(code, 60, Time.now - 30) # Verify the OTP (valid up to 2minutes)
  end

  def get_now
    @totp.now # Returns 6-digit key
  end
end