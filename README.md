# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
	> 2.4.1

* Rails version
	> 5.1.3

* System dependencies
	> Install imagemagick (installation guide is based on your OS)

* Bundle Gems
	> Open Terminal
	> Then go to app directory
	> Run:
		bundle install

* Run Application locally (given all gems and database creation was done)
	> Open Terminal
	> Then go to app directory
	> Run:
		rails server
	> Open in browser:
		http://localhost:3000 or http://127.0.0.1:3000

* Database creation
	> Update config/database.yml file
	  username: replace-this
	  password: replace-this
	> rails db:create
	> rails db:migrate

* How to run the test suite
	> Open Terminal
	> Then go to app directory
	> Run Full Test:
		minicuke or cucumber (for more details)

	> Run Specific Test:
		minicuke features/name-of-feature
		or cucumber features/name-of-feature

