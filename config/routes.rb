Rails.application.routes.draw do

  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'

  resources :users do
    collection do
      match 'setup_totp' => 'users', via: [:get, :post]
      match 'totp_recovery_code' => 'users', via: [:get, :post]
      get 'new_recory_code'
    end
  end

  resources :sessions do
    collection do
      match 'verify_totp_code' => 'sessions', via: [:get, :post]
    end
  end

  resources :password_resets
  resources :dashboard, only: [:index]

  namespace :common_user do
    match 'change_password' => 'abilities#change_password', as: 'change_password', via: [:get, :patch]
    match 'security_settings' => 'abilities#security_settings', as: 'security_settings', via: [:get, :post]
    match 'my_profile' => 'abilities#my_profile', as: 'my_profile', via: [:get, :patch]
  end
  
  namespace :admin do
    resources :users
    resources :bank_accounts do
      get 'recalculate_balance'
    end
    resources :transactions
    resources :transaction_categories
    resources :reports do
      collection do
        get 'filter_report'
        get 'individual_bank_account'
        get ':bank_account/generate_report', action: 'generate_report'
      end
    end
  end

  root to: 'sessions#new'

  match "*path", :to => "application#render_404", via: :all   # renders 404 page when routes not found
  
end