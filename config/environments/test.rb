Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # The test environment is used exclusively to run your application's
  # test suite. You never need to work with it otherwise. Remember that
  # your test database is "scratch space" for the test suite and is wiped
  # and recreated between test runs. Don't rely on the data there!
  config.cache_classes = true

  # Do not eager load code on boot. This avoids loading your whole application
  # just for the purpose of running a single test. If you are using a tool that
  # preloads Rails for running tests, you may have to set it to true.
  config.eager_load = false

  # Configure public file server for tests with Cache-Control for performance.
  config.public_file_server.enabled = true
  config.public_file_server.headers = {
    'Cache-Control' => "public, max-age=#{1.hour.seconds.to_i}"
  }

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Raise exceptions instead of rendering exception templates.
  config.action_dispatch.show_exceptions = false

  # Disable request forgery protection in test environment.
  config.action_controller.allow_forgery_protection = false
  config.action_mailer.perform_caching = false

  # Set up a canonical hostname for this instance of the app so we can refer to it in code
  config.nexus_hostname = "localhost"
  
  # Set up action mailer to deliver password resets
  config.action_mailer.default_url_options = { host: config.nexus_hostname }
  config.action_mailer.default_options = { from: "do-not-reply@#{config.nexus_hostname}" }
  
  # Tell Action Mailer not to deliver emails to the real world.
  # The :test delivery method accumulates sent emails in the
  # ActionMailer::Base.deliveries array.
  config.action_mailer.delivery_method = :test

  # Print deprecation notices to the stderr.
  config.active_support.deprecation = :stderr

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  # Set up PCI-DSS approved encryption for storing card details
  # Use the below commands to generate proper 32bytes key:
  # salt  = SecureRandom.random_bytes(32)
  # key   = ActiveSupport::KeyGenerator.new(SecureRandom.hex(32)).generate_key(salt, 32)
  config.nexus_pcidss_key = "\xDD\xEC\xDB\x80\x8Ej\xB6\xC8\xB9\a\xAE\xD8&\xE2\v\xE5g\x8A_+0\x1C\xD0A\xC0bs\xFF\xC0\x0F4\e"
end
