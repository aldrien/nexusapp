Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  if Rails.root.join('tmp/caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.seconds.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  config.action_mailer.perform_caching = false

  # Set up a canonical hostname for this instance of the app so we can refer to it in code
  config.nexus_hostname = "localhost" #REPLACE THIS
  
  # Set up action mailer to deliver password resets
  config.action_mailer.default_url_options = { host: config.nexus_hostname }
  config.action_mailer.default_options = { from: "do-not-reply@#{config.nexus_hostname}" }
  
  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Suppress logger output for asset requests.
  config.assets.quiet = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker

  # Set up PCI-DSS approved encryption for storing card details
  # Use the below commands to generate proper 32bytes key:
  # salt  = SecureRandom.random_bytes(32)
  # key   = ActiveSupport::KeyGenerator.new(SecureRandom.hex(32)).generate_key(salt, 32)
  config.nexus_pcidss_key = "K\x84{T\e\xDE\xEC\x93\x8F\xF8\xE5U\x9An4\xC4p\x81\x95\xF5\f\x11\xF1\a\x05\xFF\xE04#\xDF84"
end
