class CreateAdminTransactions < ActiveRecord::Migration[5.1]
  def up
    create_table :transactions do |t|
      t.integer :account_id
      t.integer :transaction_category_id
      t.boolean :credit
      t.integer :amount
      t.text :description
      t.datetime :transaction_date
      
      t.timestamps
    end
    add_index(:transactions, [:account_id, :transaction_category_id])
  end

  def down
    drop_table :transactions do
      remove_index :transactions, :column => [:account_id, :transaction_category_id]
    end
  end
end
