class AddColumnOpeningBalanceToBankAccounts < ActiveRecord::Migration[5.1]
  def up
    add_column :bank_accounts, :opening_balance, :decimal, precision: 10, scale: 2, default: 0.00, null: false, after: :account_number
  end

  def down
    remove_column :bank_accounts, :opening_balance, :decimal
  end
end
