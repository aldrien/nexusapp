class CreateAdminBankAccounts < ActiveRecord::Migration[5.1]
  def up
    create_table :bank_accounts do |t|
      t.integer :user_id
      t.text :description
      t.string :bank
      t.string :bank_type
      t.string :sort_code
      t.integer :account_number
      t.decimal :balance, precision: 8, scale: 2
      t.timestamps
    end
  end

  def down
    drop_table :bank_accounts
  end
end
