class AddColumnsToUsers < ActiveRecord::Migration[5.1]
  def up
    add_column :users, :activate_totp, :boolean, default: false, after: :activated
    add_column :users, :activate_u2f, :boolean, default: false, after: :activate_totp
    add_column :users, :totp_secret, :string, after: :activate_u2f
  end

  def down
    remove_column :users, :activate_totp
    remove_column :users, :activate_u2f
    remove_column :users, :totp_secret
  end
end
