class AddAttachementPhotoToUsers < ActiveRecord::Migration[5.1]
  def up
    add_column :users, :photo, :string, after: :totp_secret
  end

  def down
    remove_column :users, :photo, :string
  end
end
