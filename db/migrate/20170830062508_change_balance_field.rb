class ChangeBalanceField < ActiveRecord::Migration[5.1]
  def up
    change_column :bank_accounts, :balance, :decimal, precision: 10, scale: 2, default: 0.00, null: false
  end

  def down
    change_column :bank_accounts, :balance, :decimal, precision: 8, scale: 2
  end
end
