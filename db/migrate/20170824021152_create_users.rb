class CreateUsers < ActiveRecord::Migration[5.1]
  def up
    create_table :users do |t|
      t.string :login
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :password_digest
      t.integer :user_type_id
      t.string :password_reset_token
      t.datetime :password_reset_sent_at
      t.boolean :activated, default: true

      t.timestamps
    end
  end

  def down
    drop_table :users
  end
end
