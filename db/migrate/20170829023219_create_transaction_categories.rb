class CreateTransactionCategories < ActiveRecord::Migration[5.1]
  def up
    create_table :transaction_categories do |t|
      t.string :title
      t.text :description

      t.timestamps
    end
  end

  def down
    drop_table :transaction_categories
  end
end
