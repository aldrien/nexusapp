class AddRecoveryCodeToUsers < ActiveRecord::Migration[5.1]
  def up
    add_column :users, :totp_recovery_code, :string, after: :activate_totp
  end

  def down
    remove_column :users, :totp_recovery_code
  end
end
