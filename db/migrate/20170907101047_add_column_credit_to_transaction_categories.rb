class AddColumnCreditToTransactionCategories < ActiveRecord::Migration[5.1]
  def up
    add_column :transaction_categories, :credit, :boolean
  end

  def down
    remove_column :transaction_categories, :credit, :boolean
  end
end
