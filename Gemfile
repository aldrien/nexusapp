source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.3'
# Use mysql as the database for Active Record
gem 'mysql2', '>= 0.3.18', '< 0.5'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Use boostrap-generators
gem 'bootstrap-generators', '~> 3.3.4'
# bootstrap-generators depends functionality removed from Rails 5, record_tag_helper restores it
gem 'record_tag_helper', '~> 1.0'

# Use Font Awesome
gem 'font-awesome-rails'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem 'rotp'
gem 'rqrcode'
gem 'u2f'

gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'

gem 'will_paginate-bootstrap'

# Image upload
gem 'carrierwave'
gem 'mini_magick'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'  # WebDriver is a tool for writing automated tests of websites.
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'cucumber-rails', :require => false
  gem 'database_cleaner' # database_cleaner is not required, but highly recommended
  gem 'factory_girl_rails' # is a fixtures replacement with a straightforward definition syntax
  gem 'launchy' # Launchy is helper class for launching cross-platform applications in a fire and forget manner.
  gem 'webrat' # Webrat lets you quickly write expressive and robust acceptance tests for a Ruby web application.
  gem 'email_spec' # A collection of matchers for RSpec, MiniTest and Cucumber steps to make testing emails go smoothly.
  gem 'vcr' # Use VCR to record cassettes of remote API responses
  gem 'webmock' # or fakeweb
  gem 'delorean' # Delorean lets you travel in time with Ruby by mocking Time.now

  # gem 'rack-test'# rack-test needed to test jsonp api
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
# gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
