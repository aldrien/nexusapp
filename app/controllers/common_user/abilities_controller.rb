class CommonUser::AbilitiesController < ApplicationController
  before_action :require_login
  layout 'dashboard'

  def my_profile
    @user = current_user
    if request.patch?
      if current_user.update_attributes(user_params)
        redirect_to common_user_my_profile_path, notice: 'Your profile was successfully updated.'
      else
        render :my_profile, error: 'Sorry there was error updating profile.'
      end
    end
  end

  def change_password
    @user = current_user
    if request.patch?
      if params[:user][:password].blank? || params[:user][:password_confirmation].blank?
        @user.errors.add(:base, 'Password and confirmation cannot be blank.')
        render :change_password and return
      end

      if current_user.authenticate(params[:old_password])
        if current_user.update_attributes(user_params)
          redirect_to dashboard_index_path, notice: 'Your password has been changed.'
        else
          render :change_password, error: 'Sorry there was error updating.'
        end
      else
        @user.errors.add :old_password, "was not entered correctly."
        render :change_password
      end
    end
  end

  def security_settings
    if request.post? && params[:do_action].present?
      if current_user.authenticate(params[:password])
        if params[:do_action] == 'disable_2fa'
          current_user.update_attributes({
            activate_totp: false,
            totp_recovery_code: nil
            # totp_secret: nil
          })
          flash[:notice] = "Two Factor Authentication was successfully deactivated."
        elsif params[:do_action] == 'reset_2fa'
          current_user.update_attribute(:activate_totp, false)
          redirect_to setup_totp_users_path and return
        else
          flash[:error] = "Sorry, invalid request!"
          render :security_settings and return
        end
      else
        flash[:error] = "Sorry, invalid password. Please try again!"
      end
      render :security_settings
    end
  end

private  
  def user_params
    params.require(:user).permit(:password, :password_confirmation, :photo)
  end

  # def resolve_layout
  #   case action_name
  #   when "security_settings"
  #     "dashboard"
  #   else
  #     "application"
  #   end
  # end
end
