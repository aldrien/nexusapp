class SessionsController < ApplicationController
  include ApplicationHelper
  skip_before_action :verify_authenticity_token, only: [:create, :verify_totp_code]
  
  def index
    redirect_to root_path
  end

  def new
    redirect_to dashboard_index_path if current_user && current_user.activate_totp && session[:verifed_totp] == true
  end

  def create
    session[:verifed_totp] = false
    user = User.find_by(login: params[:login])
    if user && user.authenticate(params[:password]) && user.activated
      session[:user_email] = user.email

      # Check the Two-Factor Registration of User
      check_two_factor_authentication_registration(user)
    else
      flash.now.alert = 'Your username or password were not recognised.'
      render :new
    end
  end

  # Time-based One Time Password
  def verify_totp_code
    if session[:user_email].nil?
      flash[:error] = "You need to be logged in to get to that page."
      redirect_to new_session_path and return
    end

    @user = User.find_by(email: session[:user_email])
    rotp = RubyOneTimePassword.new(@user.totp_secret, session[:user_email])

    if params[:code].present?
      if rotp.verify(params[:code])
        do_success_auth(@user)
        session[:verifed_totp] = true
        redirect_to dashboard_index_path and return
      else
        session[:verifed_totp] = false
        do_failed_auth
      end
    elsif params[:backup_code].present?
      if decrypt(@user.totp_recovery_code) == params[:backup_code]
        # Generate and update new recovery code
        @user.update_attribute(:totp_recovery_code, encrypt(ROTP::Base32.random_base32))
        do_success_auth(@user)
        session[:verifed_totp] = true
        redirect_to new_recory_code_users_url
      else
        session[:verifed_totp] = false
        do_failed_auth
      end
    end
  end
  
  def destroy
    session[:user_id] = nil
    session[:user_type_id] = nil
    session[:user_email] = nil
    session[:verifed_totp] = nil
    redirect_to new_session_path, notice: 'You have successfully logged out.'
  end

private
  def check_two_factor_authentication_registration(current_user)
    if !current_user.activate_totp
      unless current_user.totp_secret.nil?
        do_success_auth(current_user)
        redirect_to dashboard_index_path and return
      else
        # Go Back to Initial Setup
        redirect_to setup_totp_users_path, notice: 'Please continue setting up your 2 Factor Authentication.' and return
      end
    elsif current_user.activate_totp && !current_user.activate_u2f
      # If TOTP is not configured do not ask for 2FA.
      # If TOTP is configured but U2F is not, then ask for TOTP code.
      # Go to verify QR Code page to Login
      redirect_to verify_totp_code_sessions_path, notice: 'Please continue by entering time-based code.' and return
    elsif current_user.activate_totp && current_user.activate_u2f
      # Both TOTP & U2F Device are configured
      # Then ask for U2F key to be connected and pressed
      # But also show a link to allow them to use TOTP in case they can't use their U2F key right now.
      # Go to Authenticate via U2F Device page
      redirect_to new_authentication_path, notice: 'Please connect your U2F device and press.' and return
    end
  end

  def do_success_auth(user)
    session[:user_id] = user.id
    session[:user_type_id] = user.user_type_id
    flash[:notice] = 'You have successfully logged in.'
  end

  def do_failed_auth
    flash.now.alert = 'Invalid code. Please try again!'
    render :verify_totp_code
  end

end