class Admin::ReportsController < ApplicationController
  before_action :require_login, :allow_admins_only
  before_action :filter_report, only: [:show]
  layout 'dashboard'
  require 'time'

  def index
    params[:month] ||= Time.now.month
    @transaction_categories = TransactionCategory.all
    @bank_accounts = BankAccount.includes(:bank_transactions).paginate(:page => params[:page], :per_page => 3)
    session[:report_type] = params[:type] || 'report'
  end

  def show
  end

  def filter_report
    set_category
    if params[:bank_account].present?
      @bank_account = BankAccount.find(params[:bank_account])
      @render = 'individual_bank_account'
      render :individual_bank_account
    else
      @bank_accounts = BankAccount.includes(:bank_transactions).paginate(:page => params[:page].blank? ? 1 : params[:page], :per_page => 3)
      @render = 'index'
      render :index
    end
  end

  def individual_bank_account
  end

  # https://github.com/mileszs/wicked_pdf
  # https://www.viget.com/articles/how-to-create-pdfs-in-rails
  def generate_report
    begin
      set_category
      params[:month] ||= Time.now.month
      @bank_account = BankAccount.find(params[:bank_account])
      @report_title = "#{session[:report_type].capitalize}| #{@bank_account.user.fullname}" 

      respond_to do |format|
        format.html
        format.pdf do
          render pdf:           @report_title,      # file_name
                 disposition:   Rails.env.test? ? 'inline' : 'attachment', # default 'inline' => opens in browser
                 title:         @report_title,
                 template:      'admin/reports/report.pdf.erb',
                 encoding:      'UTF-8',               
                 orientation:   'Portrait',
                 layout:        'pdf_report_template.html',
                 footer: {
                  center:    '[page] of [topage]',
                  right:     '',
                  font_name: 'Arial',
                  font_size: 8
                 },
                 margin: {                        
                  left:    10,
                  right:   10,
                  top:     10,
                  bottom:  15
                 }
        end
      end
    rescue => error
      flash[:error] = "Ooops, error happens. #{error.message}"
      redirect_to admin_reports_path
    end
  end

private
  def set_category
    if params[:category].present?
      @transaction_category = TransactionCategory.find(params[:category])
    else
      @transaction_categories = TransactionCategory.all
    end
  end
end