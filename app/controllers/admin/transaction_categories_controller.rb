class Admin::TransactionCategoriesController < ApplicationController
  before_action :require_login, :allow_admins_only
  before_action :set_transaction_category, only: [:show, :edit, :update, :destroy]
  layout 'dashboard'

  # GET /transaction_categories
  # GET /transaction_categories.json
  def index
    @admin_transaction_categories = TransactionCategory.all
  end

  # GET /transaction_categories/1
  # GET /transaction_categories/1.json
  def show
    redirect_to admin_transaction_categories_url
  end

  # GET /transaction_categories/new
  def new
    @admin_transaction_category = TransactionCategory.new
  end

  # GET /transaction_categories/1/edit
  def edit
  end

  # POST /transaction_categories
  # POST /transaction_categories.json
  def create
    @admin_transaction_category = TransactionCategory.new(transaction_category_params)

    respond_to do |format|
      if @admin_transaction_category.save
        format.html { redirect_to admin_transaction_categories_url, notice: 'Transaction category was successfully created.' }
        format.json { render :show, status: :created, location: @admin_transaction_category }
      else
        format.html { render :new }
        format.json { render json: @admin_transaction_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transaction_categories/1
  # PATCH/PUT /transaction_categories/1.json
  def update
    respond_to do |format|
      if @admin_transaction_category.update(transaction_category_params)
        format.html { redirect_to admin_transaction_categories_url, notice: 'Transaction category was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_transaction_category }
      else
        format.html { render :edit }
        format.json { render json: @admin_transaction_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transaction_categories/1
  # DELETE /transaction_categories/1.json
  def destroy
    @admin_transaction_category.destroy
    respond_to do |format|
      format.html { redirect_to admin_transaction_categories_url, notice: 'Transaction category was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction_category
      @admin_transaction_category = TransactionCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transaction_category_params
      params.require(:transaction_category).permit(:title, :description, :credit)
    end
end
