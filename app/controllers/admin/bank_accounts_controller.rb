class Admin::BankAccountsController < ApplicationController
  before_action :require_login, :allow_admins_only
  before_action :set_bank_account, only: [:show, :edit, :update, :destroy]
  layout 'dashboard'
  include ActionView::Helpers::NumberHelper
  include ApplicationHelper

  # GET /admin/bank_accounts
  # GET /admin/bank_accounts.json
  def index
    @admin_bank_accounts = BankAccount.paginate(:page => params[:page], :per_page => 10)
  end

  # GET /admin/bank_accounts/1
  # GET /admin/bank_accounts/1.json
  def show
  end

  # GET /admin/bank_accounts/new
  def new
    @admin_bank_account = BankAccount.new
  end

  # GET /admin/bank_accounts/1/edit
  def edit
  end

  # POST /admin/bank_accounts
  # POST /admin/bank_accounts.json
  def create
    @admin_bank_account = BankAccount.new(bank_account_params)
    respond_to do |format|
      if @admin_bank_account.save
        format.html { redirect_to admin_bank_accounts_url, notice: 'Bank account was successfully created.' }
        format.json { render :show, status: :created, location: @admin_bank_account }
      else
        format.html { render :new }
        format.json { render json: @admin_bank_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/bank_accounts/1
  # PATCH/PUT /admin/bank_accounts/1.json
  def update
    respond_to do |format|
      if @admin_bank_account.update(bank_account_params)
        format.html { redirect_to admin_bank_accounts_url, notice: 'Bank account was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_bank_account }
      else
        format.html { render :edit }
        format.json { render json: @admin_bank_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/bank_accounts/1
  # DELETE /admin/bank_accounts/1.json
  def destroy
    @admin_bank_account.destroy
    respond_to do |format|
      format.html { redirect_to admin_bank_accounts_url, notice: 'Bank account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def recalculate_balance
    begin
      @admin_bank_accounts = BankAccount.paginate(:page => params[:page], :per_page => 10)
      bank_account = BankAccount.find(params[:bank_account_id])
      updated_balance = bank_account.recalculate_balance
      success_msg = "#{bank_account.user.fullname} account balance was successfully updated."
      flash[:notice] = success_msg
      
      respond_to do |format|
        format.html { render :index }
        format.json {
          render json: {
            data: number_to_currency_gbp(updated_balance),
            status: :success,
            message: success_msg
          }
        }
      end

    rescue => error
      error_msg = "There was an error: #{error.message}"
      flash[:error] = error_msg
      
      respond_to do |format|
        format.html { render :index }
        format.json { render json: {status: :error, message: error_msg} }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bank_account
      @admin_bank_account = BankAccount.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bank_account_params
      params.require(:bank_account).permit(:user_id, :description, :bank, :sort_code, :account_number, :bank_type, :opening_balance, :balance)
    end
end
