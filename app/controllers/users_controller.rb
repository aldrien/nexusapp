class UsersController < ApplicationController
  before_action :require_login, except: [:new, :create]
  before_action :set_user, only: [:edit, :update, :show]
  include ApplicationHelper

  def index
    redirect_to signup_path
  end

  def edit
  end

  def show
    render :edit
  end

  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    @user.user_type_id = 1 #admin temporary
    
    if @user.save
      session[:user_id] = @user.id
      redirect_to setup_totp_users_url
    else
      render :new
    end
  end

  def setup_totp
    flash.now.notice = "Please set up your Two-Factor Authentication."
    # Checks if user's activate_totp is already activated, prevent manual page access
    if current_user.activate_totp
      flash[:error] = 'Ooops..! Forbidden page access.'
      redirect_to request.referer || root_path and return
    end

    # Generates new TOTP Secret Key for User every page load
    if request.get?
      current_user.totp_secret = ROTP::Base32.random_base32
      current_user.save
    end

    rotp = RubyOneTimePassword.new(current_user.totp_secret, current_user.email)
    @qr_png = rotp.generate_qr

    if params[:code].present?
      if rotp.verify(params[:code])
        current_user.update_attribute(:totp_recovery_code, encrypt(ROTP::Base32.random_base32))
        redirect_to totp_recovery_code_users_path
      else
        flash.now[:alert] = "Sorry that code didn't match code. Please try again!"
        render :setup_totp
      end
    end
  end

  def new_recory_code
  end

  def totp_recovery_code
    if request.post?
      current_user.update_attribute(:activate_totp, true)
      redirect_to dashboard_index_path, notice: 'Two Factor Authentication was successfully configured.' and return
    end

    flash[:notice] = 'You are one step away, please continue ...'
  end

  def update
    respond_to do |format|
      if @user.update_attributes(user_params)
        format.html { redirect_to user_path(@user), notice: 'Profile was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

private  
  def user_params
    params.require(:user).permit(:login, :first_name, :last_name, :email, :password, :password_confirmation)
  end

  def set_user
    @user = User.find(params[:id])
  end
end