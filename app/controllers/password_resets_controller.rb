class PasswordResetsController < ApplicationController
  before_action :get_user_by_token, only: [:show, :edit, :update]
  def index
    render :new
  end

  def show
    render :edit
  end

  def new
  end

  def create
    user = User.find_by_email(params[:email])
    if user
      user.send_password_reset
      redirect_to login_url, notice: "Email sent with password reset instructions."
    else
      flash[:error] = "Email address not recognised."
      render :new
    end
  end
  
  def edit
    unless @user.password_reset_sent_at > 24.hours.ago
      flash[:error] = "Password reset token has expired."
      redirect_to login_path
    end
  end
  
  def update
    if params[:user][:password].blank? || params[:user][:password_confirmation].blank?
      flash.now.alert = 'Password and confimation cannot be blank!'
      render :edit and return
    elsif params[:user][:password] != params[:user][:password_confirmation]
      flash.now.alert = 'Password and confimation not matched!'
      render :edit and return
    end
      
    if @user.password_reset_sent_at < 24.hours.ago
      redirect_to login_path, :alert => "Password reset has expired."
    elsif @user.update_attributes(user_params)
      redirect_to login_path, notice: "Password has been reset!"
    else
      render :edit
    end
  end

  private
    def user_params
      params.require(:user).permit(:password, :password_confirmation)
    end

    def get_user_by_token
      @user = User.find_by_password_reset_token!(params[:id])
    end
end