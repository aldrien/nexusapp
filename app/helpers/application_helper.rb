module ApplicationHelper
  def encryptionizer
    crypt = ActiveSupport::MessageEncryptor.new(Rails.application.config.nexus_pcidss_key)
    return crypt
  end

  def encrypt(data)
    encryptionizer.encrypt_and_sign(data)
  end

  def decrypt(data)
    encryptionizer.decrypt_and_verify(data)
  end

  def identify_user_type(type)
    case type
    when 1
      return 'Admin'
    when 2
      return 'Supervisor'
    when 3
      return 'Basic'
    else
      return 'Unknown'
    end
  end

  def number_to_currency_gbp(number)
    number_to_currency(number, unit: '£ ', precision: 2)
  end

  def number_to_currency_field(number)
    number_to_currency(number, unit: '', precision: 2)
  end

  def users_for_select
    User.all.collect { |user| [ "#{user.fullname}", user.id ] }
  end

  def user_bank_account_for_select
    BankAccount.all.collect { |b| [ "#{b.user.fullname} - #{b.account_number}", b.id ] }
  end

  def transaction_categories_for_select
    TransactionCategory.all.collect { |c| [ c.title, c.id ] }
  end

  def month_selection
    (1..12).map {|m| [Date::MONTHNAMES[m], m]}
  end

  def get_last_month_and_last_day(params_month)
    today = Date.today
    (Date.new(today.year, params_month.to_i) - 1)
  end

  def get_closing_balance_date(params_month)
    today = Date.today
    if params_month.present? && params_month.to_i != today.month
      (Date.new(today.year, params_month.to_i + 1)-1).strftime("%B %d, %Y")
    else 
      Time.now.strftime("%B %d, %Y")
    end
  end

  def get_this_month_balances(bank_account, params_month)
    if params[:category].present?
      transaction_records = bank_account.bank_transactions.records_by_month(params_month).by_category(params[:category])
    else
      transaction_records = bank_account.bank_transactions.records_by_month(params_month)
    end

    forward_balance = get_last_month_forward_balance(bank_account, params_month)

    month_credit_balance = transaction_records.get_total_credit.sum(:amount)
    month_debit_balance = transaction_records.get_total_debit.sum(:amount)
    this_month_opening_balance = forward_balance.to_f + month_credit_balance.to_f - month_debit_balance.to_f

    return {
      this_month_opening_balance:  this_month_opening_balance,
      credit_balance: month_credit_balance,
      debit_balance: month_debit_balance,
      count_credit_records:  transaction_records.get_total_credit.count,
      count_debit_records:  transaction_records.get_total_debit.count,
      transaction_records_per_category: transaction_records
    }
  end

  def get_last_month_forward_balance(bank_account, params_month)
    params_month = params_month.to_i - 1
    credit_balance = bank_account.bank_transactions.records_by_month(params_month).get_total_credit.sum(:amount)
    debit_balance = bank_account.bank_transactions.records_by_month(params_month).get_total_debit.sum(:amount)
    last_month_opening_balance = bank_account.opening_balance.to_f + credit_balance.to_f - debit_balance.to_f
  end

end