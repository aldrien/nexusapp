class BankAccount < ApplicationRecord
  belongs_to :user, optional: true
  has_many :bank_transactions, class_name: "Transaction", foreign_key: "account_id", dependent: :destroy

  validates_presence_of :user_id, :description, :bank, :bank_type, :opening_balance, :balance
  # validates_numericality_of :opening_balance, :greater_than => 0, only_integer: false
  # validates :opening_balance, presence: true, numericality: { greater_than: 0, only_integer: false }
  validates_uniqueness_of :account_number unless Rails.env.test?
  validates_length_of :account_number, :within => 8..10, message: "format is invalid", allow_blank: true

  before_validation do
    self.opening_balance = opening_balance.to_s.gsub(",", "").to_f unless opening_balance.blank?
    self.balance = balance.to_s.gsub(",", "").to_f unless balance.blank?
  end

  def recalculate_balance
    # Formula: new_balance = opening_balance + (credits - debits)
    new_balance = (self.opening_balance + self.bank_transactions.get_total_credit.sum(:amount)) - self.bank_transactions.get_total_debit.sum(:amount)
    self.update_attribute(:balance, new_balance)

    return new_balance
  end
end