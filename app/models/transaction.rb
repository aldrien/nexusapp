class Transaction < ApplicationRecord
  belongs_to :bank_account, foreign_key: "account_id"
  has_one :user, through: :bank_account
  belongs_to :transaction_category, optional: true

  before_save :set_credit
  after_create :update_bank_account_balance

  validates_presence_of :account_id, :amount, :description, :transaction_category_id, :transaction_date

  scope :get_total_credit, -> { where(credit: true) }
  scope :get_total_debit, -> { where(credit: false) }

  scope :records_of_this_month, -> { where(transaction_date: Time.now.beginning_of_month..Time.now.end_of_month) }
  scope :by_category, -> (category_id) { where(transaction_category_id: category_id) if category_id.present? }
  scope :by_user, -> (user_id) { where(account_id: user_id) if user_id.present? }

  # scope :created_in, -> (month) { where( "MONTH( transaction_date ) = ?", month ) if month.present? }
  scope :records_by_month, -> (month_params) { where('extract(month from transaction_date) = ?', month_params) if month_params.present?}
  
  def set_credit
    transac_category = TransactionCategory.find(self.transaction_category_id)
    # true = credit, false = debit
    if transac_category.credit
      self.credit = true
    else
      self.credit = false
    end
  end

  def update_bank_account_balance
    if self.credit?
      new_balance = self.bank_account.balance.to_f + self.amount.to_f
    else
      new_balance = self.bank_account.balance.to_f - self.amount.to_f
    end
    self.bank_account.update_attribute(:balance, new_balance)
  end
    
end