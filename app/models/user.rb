class User < ApplicationRecord
  has_secure_password
  has_one :bank_account, dependent: :destroy
  has_many :bank_transactions, through: :bank_account, dependent: :destroy

  validates_presence_of :login, :first_name, :last_name, :email
  validates_format_of :email, :with => /\A[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}\Z/i

  validates_uniqueness_of :login, message: "Username has already registered."
  validates_uniqueness_of :email

  mount_uploader :photo, PhotoUploader

  def is_admin?
    self.user_type_id == 1
  end

  def fullname
    "#{first_name.capitalize} #{last_name.capitalize}"
  end

  def send_password_reset
    generate_token(:password_reset_token)
    self.password_reset_sent_at = Time.zone.now
    save!
    ApplicationMailer.password_reset(self).deliver_now
  end

  def generate_token(column)
    begin
      self[column] = SecureRandom.hex
    end while User.exists?(column => self[column])
  end
end