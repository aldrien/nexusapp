class TransactionCategory < ApplicationRecord
  has_many :bank_transactions, class_name: 'Transaction'
  validates_presence_of :title, :description

  scope :income_categories, -> {where(credit: true)}
  scope :expense_categories, -> {where(credit: false)}
end
