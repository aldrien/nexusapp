json.extract! admin_transaction_category, :id, :title, :description, :created_at, :updated_at
json.url admin_transaction_category_url(admin_transaction_category, format: :json)
