// Setting Active Nav
$(function(){
  // Remove active nav first
  $('li.nav-item').removeClass('active');
  $('div.arrow-right').remove();

  // Check page and set active nav
  if(page.split('/')[1] == undefined){
    $('li#'+page).addClass('active').prepend('<div class="arrow-right"></div>');
  }else{
    // For Common User Management
    if(page == 'common_user/abilities'){
      $('li#'+page.concat(action).split('/')[1]).addClass('active').prepend('<div class="arrow-right"></div>');
    }else{
      // For Admin Management
      var admin_page = page.split('/')[1];
      var active_li = '';
      switch(admin_page) {
      case 'users':
        active_li = 'users';
        break;
      case 'bank_accounts':
        active_li = 'bank_accounts';
        break;
      case 'transactions':
        active_li = 'transactions';
        break;
      case 'transaction_categories':
        active_li = 'transaction_categories';
        break;
      case 'reports':
        active_li = 'reports';
        // For dropdown navigation
        $('li#reports').addClass('open');
        $("#"+sub_nav).addClass('sub-nav-active')
        break;
      default:
        active_li = 'dashboard';
      }

      $('li#'+ active_li).addClass('active').prepend('<div class="arrow-right"></div>');
    }
  }
});

// Prevent dropdown from closing when clicking other element
// Only in report page
if(page == "admin/reports"){
  $('body').on('click', function (e) {
    if (!$('li.dropdown').is(e.target) 
      && $('li.dropdown').has(e.target).length === 0 
      && $('.open').has(e.target).length === 0
    ) {
      e.stopPropagation();
    }
  });
}